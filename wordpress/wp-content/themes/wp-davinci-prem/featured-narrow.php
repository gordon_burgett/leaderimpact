<div id="home-top-narrow" class="clearfix">

	<div id="slideshow" class="clearfix">

		<?php if (get_option('solostream_features_title')) { ?>
		<h2 class="feature-title"><span><?php echo stripslashes(get_option('solostream_features_title')); ?></span></h2>
		<?php } ?>

		<div class="slides clearfix">

			<ul class="clearfix">

<?php 
$count = 1;
$featurecount = get_option('solostream_features_number'); 
$my_query = new WP_Query("tag=featured&showposts=$featurecount");
while ($my_query->have_posts()) : $my_query->the_post();
$do_not_duplicate[] = $post->ID; ?>

				<li id="main-post-<?php echo $count; ?>">

					<div class="entry clearfix">

						<a href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "solostream"); ?>" title="<?php _e("Permanent Link to", "solostream"); ?> <?php the_title(); ?>"><?php include (TEMPLATEPATH . "/post-thumb.php"); ?></a>

						<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "solostream"); ?>" title="<?php _e("Permanent Link to", "solostream"); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>

						<?php if ( get_option('solostream_post_content') == 'Excerpts' ) { ?>
						<?php the_excerpt(); ?>
						<?php } else { ?>
						<?php the_content(''); ?>
						<?php } ?>

					</div>

					<?php include (TEMPLATEPATH . "/postinfo.php"); ?>

					<div style="clear:both;"></div>

				</li>

<?php $count = $count + 1 ?>
<?php endwhile; ?>

			</ul>

		</div>

		<ul class="slides-nav">

<?php 
$count = 1;
$featurecount = get_option('solostream_features_number'); 
$my_query = new WP_Query("tag=featured&showposts=$featurecount");
while ($my_query->have_posts()) : $my_query->the_post();
$do_not_duplicate[] = $post->ID; ?>

			<li id="nav-post-<?php echo $count; ?>" class="clearfix<?php if ( $count == 1 ) { ?> on<?php } ?>">
				<a href="#main-post-<?php echo $count; ?>" title="<?php the_title(); ?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif" alt="<?php the_title(); ?>" align="top" />
				</a>
			</li>

<?php $count = $count + 1 ?>
<?php endwhile; ?>

		</ul>

	</div>

</div>