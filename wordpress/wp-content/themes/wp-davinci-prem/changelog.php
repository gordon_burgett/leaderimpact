*** WP-DaVinci Theme Framework Changelog ***

01/18/2011 - Verison 1.0.1

* style.css - fixed IE6 bug on #topnav #searchform.
* style.css - fixed IE6 bug on .meta a.more-link.
* style.css - p#breadcrumbs changed to height:auto.
* style.css - full-width slider bug fixed (.full-width .entry height:auto).
* style.css - added z-index:99999 to #topnav .limit to fix IE bug.
* author.php - deleted duplicate call to banner468.php.
* search.php - deleted duplicate call to banner468.php.
* theme-settings.php - modified capability level for solostream theme settings page.
* theme-settings.php - fixed bug causing theme settings to be deleted on theme change.
* auth-bio.php - modified author description to include HTML.
* auth-bio.php - added ability to hide author bio on a post-by-post basis.
* theme-metaboxes.php - added hide_auth_bio meta box.
* page-tabbed-archive.php - overhauled the 3 different loops to be more efficient.
* featured-wide.php - fixed validation errors caused by <span> tag.
* featured-galleries.php - fix IE7 js error caused by extra comma.
* featured-vids.php - fix IE7 js error caused by extra comma.
* theme-js.php - added line to call suckerfish-cat.js only when cat nav activated.
* suckerfish.js - removed catnav section from file.
* suckerfish-cat.js - added file.
* single.php - added wp_link_pages function for paginated posts.
* page.php - added wp_link_pages function for paginated pages.

11/23/2010 - Version 1.0.0
* First Logged release