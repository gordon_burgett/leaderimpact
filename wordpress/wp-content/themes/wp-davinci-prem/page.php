<?php get_header(); ?>

	<?php 
	global $wp_query;
	$postid = $wp_query->post->ID;
	 if ( get_post_meta( $postid, 'post_featcontent', true ) == "Full Width Featured Content Slider"  ) { ?>
		<?php include (TEMPLATEPATH . '/featured-wide.php'); ?>
	<?php } ?>

	<div id="page" class="clearfix">

		<div id="contentleft">

			<?php if ( get_post_meta( $postid, 'post_featcontent', true ) == "Narrow Width Featured Content Slider" ) { ?>
				<?php include (TEMPLATEPATH . '/featured-narrow.php'); ?>
			<?php } ?>

			<?php if ( get_post_meta( $postid, 'post_featvideo', true ) == "Yes" ) { ?>
				<?php include (TEMPLATEPATH . '/featured-vids.php'); ?>
			<?php } ?>

			<?php if ( get_post_meta( $postid, 'post_featgalleries', true ) == "Yes" ) { ?>
				<?php include (TEMPLATEPATH . '/featured-galleries.php'); ?>
			<?php } ?>

			<div id="content" class="maincontent">

				<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>	

				<?php include (TEMPLATEPATH . '/banner468.php'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="singlepage">

					<div class="post clearfix" id="post-main-<?php the_ID(); ?>">

						<div class="entry">

							<h1 class="page-title"><?php the_title(); ?></h1>

							<?php $data = get_post_meta( $post->ID, 'Solostream', true );
							if ($data['video_embed']) { ?>
							<div class="single-video">
								<?php $embed = $data['video_embed'];
								if ( $data['layout'] && $data['layout'] !== 'Default' ) { 
									$layout = $data['layout']; 
								} else { 
									$layout = get_option('solostream_layout'); 
								}
								$alt = preg_match_all('/(width|height)=("[^"]*")/i', $embed, $matches);
								if ( $layout == "Content | Sidebar-Narrow | Sidebar-Wide" || $layout == "Sidebar-Narrow | Content | Sidebar-Wide" || $layout == "Sidebar-Wide | Sidebar-Narrow | Content" || $layout == "Sidebar-Wide | Content | Sidebar-Narrow" ) { 
									$embed = preg_replace('/(width)=("[^"]*")/i', 'width="470"', $embed);
									$embed = preg_replace('/(height)=("[^"]*")/i', 'height="350"', $embed);
								} elseif  ( $layout == "Full-Width" ) {
									$embed = preg_replace('/(width)=("[^"]*")/i', 'width="950"', $embed);
									$embed = preg_replace('/(height)=("[^"]*")/i', 'height="600"', $embed);
								} else {
									$embed = preg_replace('/(width)=("[^"]*")/i', 'width="620"', $embed);
									$embed = preg_replace('/(height)=("[^"]*")/i', 'height="400"', $embed);
								}
								echo $embed;
								?>
							</div>
							<?php } ?>

							<?php the_content(); ?>

							<div style="clear:both;"></div>

							<?php wp_link_pages(); ?>

						</div>

					</div>


				</div>

<?php endwhile; endif; ?>
				
			</div>

			<?php include (TEMPLATEPATH . '/sidebar-narrow.php'); ?>

		</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>