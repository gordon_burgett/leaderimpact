<div class="meta<?php if ( is_single() ) { ?> single<?php } ?>">

	<?php if ( is_single() ) { ?>
	<span class="meta-author">
		<?php the_author_posts_link(); ?> 
	</span> 

	| 
	<?php } ?>
	<span class="meta-date">
		<?php the_time( get_option( 'date_format' ) ); ?>
	</span> 

	<?php if ('open' == $post->comment_status) { ?>
	| 
	<span class="meta-comments">
		<a href="<?php comments_link(); ?>" rel="<?php _e("bookmark", "solostream"); ?>" title="<?php _e("Comments for", "solostream"); ?> <?php the_title(); ?>"><?php comments_number(__("0 Comments", "solostream"), __("1 Comment", "solostream"), __("% Comments", "solostream")); ?></a>
	</span>
	<?php } ?> 

	<?php if ( !is_single() ) { ?>
	<a class="more-link" href="<?php the_permalink() ?>" rel="nofollow" title="<?php _e("Permanent Link to", "solostream"); ?> <?php the_title(); ?>"><?php _e("More", "solostream"); ?></a>
	<?php } ?> 

</div>
