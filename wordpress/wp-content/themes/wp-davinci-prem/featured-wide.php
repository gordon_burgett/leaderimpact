<div id="home-top" class="clearfix">

	<div id="slideshow" class="clearfix">

		<?php if (get_option('solostream_features_title')) { ?>
		<h2 class="feature-title"><span><?php echo stripslashes(get_option('solostream_features_title')); ?></span></h2>
		<?php } ?>

		<div class="slides clearfix">

			<ul class="clearfix">

<?php 
$count = 1;
$featurecount = get_option('solostream_features_number'); 
$my_query = new WP_Query("tag=featured&showposts=$featurecount");
while ($my_query->have_posts()) : $my_query->the_post();
$do_not_duplicate[] = $post->ID; ?>

				<li id="main-post-<?php echo $count; ?>"<?php if ( has_tag('full-image') ) { ?> class="full-width"<?php }?>>

					<div class="entry clearfix">

<?php $data = get_post_meta( $post->ID, 'video_embed', true );
if ($data) { ?>

						<div class="feature-video">
							<?php
							$alt = preg_match_all('/(width|height)=("[^"]*")/i', $data, $matches);
							$data = preg_replace('/(width)=("[^"]*")/i', 'width="375"', $data);
							$data = preg_replace('/(height)=("[^"]*")/i', 'height="250"', $data);
							echo $data;
							?>
						</div>

<?php } else { ?>

						<?php
						if ( get_option('solostream_default_features') == 'yes' && has_tag('full-image') ) { 
							$defthumb = get_bloginfo('stylesheet_directory') . '/images/def-thumb3.jpg'; 
						} elseif ( get_option('solostream_default_features') == 'yes' && !has_tag('full-image') ) { 
							$defthumb = get_bloginfo('stylesheet_directory') . '/images/def-thumb2.jpg'; 
						} else { 
							$defthumb == 'false'; 
						}
						if ( function_exists('get_the_image') ) { ?>
						<div class="feature-image">
							<?php get_the_image(array(
								'custom_key' => array('home_feature','feature', 'home_feature_wide'),
								'default_size' => 'full',
								'default_image' => $defthumb,
								'link_to_post' => true,
								'image_scan' => true,
							)); ?>
						</div>
						<?php } ?>

<?php } ?>

						<div class="wide-feat-content">
							<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="<?php _e("bookmark", "solostream"); ?>" title="<?php _e("Permanent Link to", "solostream"); ?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>

							<div class="meta">
								<?php the_time( get_option( 'date_format' ) ); ?><?php if ('open' == $post->comment_status) { ?> | <a href="<?php comments_link(); ?>" rel="<?php _e("bookmark", "solostream"); ?>" title="<?php _e("Comments for", "solostream"); ?> <?php the_title(); ?>"><?php comments_number(__("0 Comments", "solostream"), __("1 Comment", "solostream"), __("% Comments", "solostream")); ?></a><?php } ?> | <a class="more-link" href="<?php the_permalink() ?>" rel="nofollow" title="<?php _e("Permanent Link to", "solostream"); ?> <?php the_title(); ?>"><?php _e("More", "solostream"); ?></a>
							</div>

							<?php if ( get_option('solostream_post_content') == 'Excerpts' ) { ?>
							<?php the_excerpt(); ?>
							<?php } else { ?>
							<?php the_content(''); ?>
							<?php } ?>
						</div>

					</div>

					<div style="clear:both;"></div>

				</li>

<?php $count = $count + 1 ?>
<?php endwhile; ?>

			</ul>

		</div>

		<ul class="slides-nav">

<?php 
$count = 1;
$featurecount = get_option('solostream_features_number'); 
$my_query = new WP_Query("tag=featured&showposts=$featurecount");
while ($my_query->have_posts()) : $my_query->the_post();
$do_not_duplicate[] = $post->ID; ?>

			<li id="nav-post-<?php echo $count; ?>" class="clearfix<?php if ( $count == 1 ) { ?> on<?php } ?>">
				<a href="#main-post-<?php echo $count; ?>" title="<?php the_title(); ?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif" alt="<?php the_title(); ?>" align="top" />
				</a>
			</li>

<?php $count = $count + 1 ?>
<?php endwhile; ?>

		</ul>

	</div>

</div>