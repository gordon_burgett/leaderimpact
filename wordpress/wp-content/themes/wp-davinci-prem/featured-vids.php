<div id="feature-vids">

	<?php if (get_option('solostream_videos_title')) { ?>
	<h2 class="feature-title"><span><?php echo stripslashes(get_option('solostream_videos_title')); ?></span></h2>
	<?php } ?>

	<div id="slideshowfeaturevids" class="clearfix">

		<div class="slides clearfix">

			<ul class="clearfix">

<?php 
$count = 1;
$my_query = new WP_Query(array(
	'category_name' => get_option('solostream_videos_cat'),
	'showposts' => get_option('solostream_videos_count')
));
while ($my_query->have_posts()) : $my_query->the_post();
$do_not_duplicate[] = $post->ID;
$embed = get_post_meta( $post->ID, 'video_embed', true ); ?>

				<li id="main-vid-post-<?php echo $count; ?>">
					<div class="feature-vid">
						<?php 
						$alt = preg_match_all('/(width|height)=("[^"]*")/i', $embed, $matches);
						$embed = preg_replace('/(width)=("[^"]*")/i', 'width="610"', $embed);
						$embed = preg_replace('/(height)=("[^"]*")/i', 'height="300"', $embed);
						echo $embed;
						?>
					</div>
				</li>

<?php $count = $count + 1 ?>
<?php endwhile; ?>

			</ul>

		</div>

<script type="text/javascript">
//<![CDATA[
	jQuery(document).ready(function(){
		jQuery('#slider-nav').navslide({ 
			display: 1, // how many slides do you want to move at 1 time?
			interval: false, // auto-slide.
			intervaltime: 5000, // auto-slide in milliseconds.
			duration: 500, // how fast must should slides move in ms?
		});
	});
//]]>
</script>

		<div id="slider-nav">

			<div class="slidenav">
				<strong>More <?php echo stripslashes(get_option('solostream_videos_title')); ?></strong>
				<?php if ( get_option('solostream_videos_count') != "3" ) { ?>
				<a class="buttons next" href="#">&raquo</a><a class="buttons prev" href="#">&laquo;</a>
				<?php } ?>
			</div>

			<div class="slideport">

				<ul class="slideview slides-nav">

<?php 
$count = 1;
$my_query = new WP_Query(array(
	'category_name' => get_option('solostream_videos_cat'),
	'showposts' => get_option('solostream_videos_count')
));
while ($my_query->have_posts()) : $my_query->the_post();
$do_not_duplicate[] = $post->ID; ?>

					<li class="clearfix<?php if ( $count == 1 ) { ?> on<?php } ?>">

						<a class="slider-thumb" href="#main-vid-post-<?php echo $count; ?>" title="<?php _e("Watch", "solostream"); ?> <?php the_title(); ?>"><?php include (TEMPLATEPATH . "/post-thumb.php"); ?></a>
					
						<div>
							<strong><?php the_title(); ?></strong>
							<?php the_excerpt(); ?>
						</div>

						<div class="view" title="<?php _e("Permanent Link to", "solostream"); ?> <?php the_title(); ?>" onclick="location.href='<?php the_permalink() ?>';">
							<?php _e("View Post", "solostream"); ?>
						</div>					
					</li>

<?php $count = $count + 1 ?>
<?php endwhile; ?>

				</ul>

			</div>

		</div>

	</div>

</div>