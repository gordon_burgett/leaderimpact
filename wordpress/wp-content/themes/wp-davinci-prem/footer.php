	</div>

<?php /* footer widgets */ if ( is_active_sidebar('widget-5') || is_active_sidebar('widget-6') || is_active_sidebar('widget-7') || is_active_sidebar('widget-8') ) { ?>
	<?php include (TEMPLATEPATH . '/banner728-bottom.php'); ?>

	<div id="footer-widgets" class="maincontent">

		<div class="limit clearfix">
			<div class="footer-widget1">
				<?php dynamic_sidebar('Footer Widget 1'); ?>
			</div>
			<div class="footer-widget2">
				<?php dynamic_sidebar('Footer Widget 2'); ?>
			</div>
			<div class="footer-widget3">
				<?php dynamic_sidebar('Footer Widget 3'); ?>
			</div>
			<div class="footer-widget4">
				<?php dynamic_sidebar('Footer Widget 4'); ?>
			</div>
		</div>

	</div>
<?php } ?>

<?php if ( !is_active_sidebar('widget-5') && !is_active_sidebar('widget-6') && !is_active_sidebar('widget-7') && !is_active_sidebar('widget-8') ) { ?>
	<?php include (TEMPLATEPATH . '/banner728-bottom.php'); ?>
<?php } ?>

	<div id="footer">

		<div class="limit clearfix">

			&copy; <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a> <?php echo date('Y'); ?>. <?php _e("All rights reserved.", "solostream"); ?>

			<?php /* Solostream Footer Credit. You can only remove this if you own the Premium License */ $link = array(
			"<a href = \"http://www.solostream.com\">WordPress Themes</a>",
			"<a href = \"http://www.solostream.com\">Premium WordPress Themes</a>",
			"<a href = \"http://www.solostream.com\">WordPress Business Themes</a>",
			"<a href = \"http://www.solostream.com\">Business WordPress Themes</a>",
			"<a href = \"http://www.wp-magazine.com\">WordPress Magazine Themes</a>"); 
			srand(time()); 
			$random = (rand()%5); 
			echo ("$link[$random]");
			/* End Solostream Footer Credit. */ ?>

			<?php include (TEMPLATEPATH . '/sub-icons.php'); ?>

		</div>

	</div>

</div>

</div>

<?php wp_footer(); ?>

</body>

</html>