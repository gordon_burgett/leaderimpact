<?php
$data = get_post_meta( $post->ID, 'remove_thumb', true );
if ( get_option('solostream_default_thumbs') == 'yes' ) { $defthumb = get_bloginfo('stylesheet_directory') . '/images/def-thumb.jpg'; } else { $defthumb == 'false'; }
if ( get_option('solostream_show_thumbs') == 'yes' && function_exists('get_the_image') && $data != 'Yes' ) { ?>
<?php get_the_image(array(
	'custom_key' => array('post_thumbnail','thumbnail'),
	'default_size' => 'thumbnail',
	'default_image' => $defthumb,
	'image_scan' => true,
	'link_to_post' => false,
)); ?>
<?php } ?>
