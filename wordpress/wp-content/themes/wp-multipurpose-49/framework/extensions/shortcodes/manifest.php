<?php if (!defined('FW')) die('Forbidden');

$manifest = array(
	'version'       => '1.2.9',
	'display'       => false,
	'standalone'    => true,
	'requirements'  => array(
		'extensions'  => array(
			'builder' => array(),
		),
	),
	'github_update' => ''
);
