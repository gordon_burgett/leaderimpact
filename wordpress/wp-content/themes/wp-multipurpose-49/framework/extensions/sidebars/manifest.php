<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'Sidebars', 'fw' );
$manifest['description'] = __( 'Brings a new layer of customization freedom to your website by letting you add more than one sidebar to a page, or different sidebars on different pages.', 'fw' );
$manifest['version'] = '1.0.2';
$manifest['display'] = true;
$manifest['standalone']   = true;

$manifest['github_update'] = '';
