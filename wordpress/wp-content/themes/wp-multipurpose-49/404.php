<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area <?php with_sidebar(); ?>">
		<div id="content" class="site-content" role="main">

			<header class="page-header fw-container">
				<h2 class="page-title"><?php _e( 'Not Found', 'unyson' ); ?></h2>
				<?php
				if( function_exists('fw_ext_breadcrumbs') ) {
					fw_ext_breadcrumbs();
				}
				?>
			</header>

			<div class="page-content fw-container">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'unyson' ); ?></p>

				<?php get_search_form(); ?>
			</div><!-- .page-content -->

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php	get_sidebar(); ?>

</div><!-- #main-content -->

<?php
get_footer();
