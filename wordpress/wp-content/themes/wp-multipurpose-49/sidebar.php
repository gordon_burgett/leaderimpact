<?php
/**
 * The Sidebar containing the main widget area
 */
?>
	
	<?php 
		$current_position = sidebar_curr_position();
	?>

	<div id="sidebar" class="sidebar <?php echo $current_position; ?> widget-area" role="complementary">
		
	<?php if ( $current_position !== 'full' ) : ?>
		<?php if ($current_position === 'left' or $current_position === 'right' ) : ?>
			<?php echo fw_ext_sidebars_show('blue'); ?>
		<?php endif; ?>
		<?php if ( $current_position === 'left_right' ) :?>
			<?php return; ?>
		<?php endif; ?>
	<?php endif; ?>

	</div>
