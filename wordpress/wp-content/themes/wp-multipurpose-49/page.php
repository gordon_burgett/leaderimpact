<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area <?php with_sidebar(); ?>">
		<div id="content" class="site-content " role="main">
			<?php
				$body = get_body_class();
				if ((in_array('woocommerce-page', $body)) || (in_array('bbpress', $body))){
					echo '<div class="fw-container">';
				}
			?>

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
			?>

			<?php

			if ((in_array('woocommerce-page', $body)) || (in_array('bbpress', $body))){
				echo '</div>';
			}
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php	get_sidebar(); ?>

</div><!-- #main-content -->

<?php
get_footer();
