<?php
/**
 * The template for displaying posts in the Aside post format
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(array('fw-container')); ?>>
		<div class="fw-row">
			<div class="fw-col-sm-3">
			<?php
			if ( has_post_thumbnail() ) : ?>
				<?php
				the_post_thumbnail( 'medium', array('class' => 'thumb') );
				?>
			<?php endif; ?>
			</div>
		<div class="fw-col-sm-9">
			<div class="entry-content">
				<?php
					the_content();
				?>
			</div><!-- .entry-content -->
			<div class="entry-meta">
					<span class="post-format">
						<a class="entry-format" href="<?php echo esc_url( get_post_format_link( 'aside' ) ); ?>" title="<?php echo get_post_format_string( 'aside' );?>"><span class="fa fa-file-text-o"></span></a>
					</span>

				<?php fw_theme_posted_on(); ?>

				<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
					<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'unyson' ), __( '1 Comment', 'unyson' ), __( '% Comments', 'unyson' ) ); ?></span>
				<?php endif; ?>

				<?php edit_post_link( __( 'Edit', 'unyson' ), '<span class="edit-link">', '</span>' ); ?>
			</div><!-- .entry-meta -->

			<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
		</div>
		</div>
	</article><!-- #post-## -->
