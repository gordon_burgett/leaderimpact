<?php
/**
 * The template for displaying posts in the Quote post format
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('fw-container'); ?>>
  <div class="fw-row">

    <div class="quote-avatar">

    <?php
    if ( has_post_thumbnail() ) : ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php
        the_post_thumbnail('avatar');
        ?>
      </a>
    <?php endif; ?>
    </div>
    <div class="quote-content">
    <header class="entry-header">
      <?php

      if ( is_single() ) :
        the_title( '<h3 class="entry-title"><span class="fa fa-quote-left"></span> ', '</h3>' );
      else :
        the_title( '<h3 class="entry-title"><span class="fa fa-quote-left"></span> <a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
      endif;
      ?>
      <?php
      if( function_exists('fw_ext_breadcrumbs') && is_single() ) {
        fw_ext_breadcrumbs();
      }
      ?>


    </header><!-- .entry-header -->

    <div class="entry-content">
      <?php
      the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'unyson' ) );
      wp_link_pages( array(
        'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'unyson' ) . '</span>',
        'after'       => '</div>',
        'link_before' => '<span>',
        'link_after'  => '</span>',
      ) );
      ?>
    </div><!-- .entry-content -->

      <?php
      if ( is_single() ) :
        ?>

        <div class="entry-meta">
        <span class="post-format">
          <a class="entry-format" href="<?php echo esc_url( get_post_format_link( 'quote' ) ); ?>" title="<?php echo get_post_format_string( 'quote' );?>"><span class="fa fa-quote-left"></span></a>
        </span>

          <?php fw_theme_posted_on(); ?>

          <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
            <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'unyson' ), __( '1 Comment', 'unyson' ), __( '% Comments', 'unyson' ) ); ?></span>
          <?php endif; ?>

          <?php edit_post_link( __( 'Edit', 'unyson' ), '<span class="edit-link">', '</span>' ); ?>
        </div><!-- .entry-meta -->
      <?php
      endif;
      ?>

    <?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
    </div>
  </div>
</article><!-- #post-## -->
