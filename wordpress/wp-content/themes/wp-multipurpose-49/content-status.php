<?php
/**
 * The template for displaying posts in the Status post format
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(array('fw-container')); ?>>
		<div class="fw-row">
			<?php	if ( is_single() ) : ?>
			<div>
			<?php	else : ?>
				<div class="status-avatar">
				<?php
					if ( has_post_thumbnail() ) :
						the_post_thumbnail( 'avatar' );
					endif;
				?>
			</div>
		<div class="status-content">
			<?php
			endif;
			?>

				<header class="entry-header">
					<?php
					if( function_exists('fw_ext_breadcrumbs') && is_single() ) {
						fw_ext_breadcrumbs();
					}
					?>
					<?php
					if ( is_single() ) :
						the_title( '<h3 class="entry-title">', '</h3>' );
					else :
						the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
					endif;
					?>
					<div class="entry-meta">
						<span class="post-format">
							<a class="entry-format" href="<?php echo esc_url( get_post_format_link( 'status' ) ); ?>" title="<?php echo get_post_format_string( 'status' );?>"><span class="fa fa-pencil"></span></a>
						</span>

						<?php fw_theme_posted_on(); ?>

						<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
							<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'unyson' ), __( '1 Comment', 'unyson' ), __( '% Comments', 'unyson' ) ); ?></span>
						<?php endif; ?>

						<?php edit_post_link( __( 'Edit', 'unyson' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-meta -->

				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php
						the_content();;
					?>
				</div><!-- .entry-content -->


				<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
			</div>
		</div>
	</article><!-- #post-## -->
