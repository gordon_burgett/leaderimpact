<?php
/**
 * The Footer Sidebar
 */

if ( ! is_active_sidebar( 'sidebar-footer-small' ) ) {
  return;
}
?>

<div id="colophon" class="section site-info" role="complementary">
  <div class="container">
    <div class="row">
      <?php dynamic_sidebar( 'sidebar-footer-small' ); ?>
    </div>
  </div>
</div><!-- #footer-sidebar -->