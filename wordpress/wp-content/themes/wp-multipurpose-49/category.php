<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area <?php with_sidebar(); ?>">
		<div id="content" class="site-content" role="main">

			<?php if ( have_posts() ) : ?>

			<header class="archive-header fw-container">
				<h2 class="archive-title"><?php printf( __( 'Category Archives: %s', 'unyson' ), single_cat_title( '', false ) ); ?></h2>
				<?php
				if( function_exists('fw_ext_breadcrumbs') ) {
					fw_ext_breadcrumbs();
				}
				?>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .archive-header -->

			<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next page navigation.
				?>
				<div class="fw-container">
					<?php fw_theme_paging_nav(); ?>
				</div>
				<?php

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
<?php	get_sidebar(); ?>

	</div><!-- #main-content -->

<?php
get_footer();
