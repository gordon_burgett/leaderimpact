<?php
/**
 * The template used for displaying page content
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php
		if ( has_post_thumbnail() ) : ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php
				the_post_thumbnail('full', array('class' => 'thumb') );
				?>
			</a>
		<?php endif; ?>


	<?php
	if( !is_front_page() && function_exists('fw_ext_breadcrumbs') && is_page() ) {
		fw_ext_breadcrumbs();
	}
	?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'unyson' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );


		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
<div class="fw-container">
	<?php
		edit_post_link( __( 'Edit', 'unyson' ), '<span class="edit-link page">', '</span>' );
	?>
</div>