<?php
/**
 * The template for displaying posts in the Link post format
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('fw-container'); ?>>


		<?php
		if ( has_post_thumbnail() ) : ?>
	<div class="image-container">
			<a href="<?php echo get_post_link_url(); ?>" title="<?php the_title_attribute(); ?>">
				<?php
				the_post_thumbnail('link-posts-image', array('class' => 'logo') );
				?>
			</a>
	</div>
	<div class="content-container">
		<?php else : ?>
		<div>
		<?php endif; ?>
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h3 class="entry-title">', '</h3>' );
			else :
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			endif;
			?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
				the_content();
			?>
		</div><!-- .entry-content -->
		<div class="pull-right">
			<?php edit_post_link( __( 'Edit', 'unyson' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
	</div>
</article><!-- #post-## -->
