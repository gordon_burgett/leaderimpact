<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 */
?>
</div>




</div><!-- #page -->

<div class="footer">
  <?php get_sidebar( 'footer' ); ?>

  <?php get_sidebar( 'footer-small' ); ?>
</div>
<?php wp_footer(); ?>
	<?php if( defined('FW') ) : ?>
		<div id="flash-messages"><?php FW_Flash_Messages::_print_frontend(); ?></div>
		<script type="text/javascript">
			jQuery(function ($) {
				$('#flash-messages').prependTo( $('#content') );
			});
		</script>
	<?php endif ?>
</body>
</html>