<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Register menus
 */

// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
	'primary'   => __( 'Main Navigation', 'unyson' ),
	'top_bar_menu' => __('Top Bar', 'unyson')
//	'secondary' => __( 'Secondary menu in left sidebar', 'unyson' ),
) );