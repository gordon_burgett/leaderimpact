<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * @var $instance
 * @var $before_widget
 * @var $after_widget
 * @var $title
 */


?>
<?php if ( ! empty( $instance ) ) : ?>
	<?php echo $before_widget ?>
	<div class="wrap-social">
		<?php echo $title; ?>
			<?php foreach ( $instance as $key => $value ) :
				if ( empty( $value ) ) {
					continue;
				}
				?>
					<a href="<?php echo esc_attr( $value ); ?>" class="social" title="<?php echo $key ?>" target="_blank">
						<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-<?php echo $key ?> fa-stack-1x fa-inverse dark-blue"></i>
						</span>
					</a>
			<?php endforeach; ?>
	</div>
	<?php echo $after_widget ?>
<?php endif; ?>