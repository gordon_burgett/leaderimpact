<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * @var $instance
 * @var $before_widget
 * @var $after_widget
 * @var $title
 */


?>
<?php
$types = array('phone', 'mail', 'url', 'location');
?>
	<?php $isGetFromTheme = $instance['src']; ?>
	<?php echo $before_widget ?>
	<div class="wrap-contacts">
		<?php echo $title; ?>
		<ul>

			<?php
			if ($isGetFromTheme) {
				foreach ($types as $type) :
					?>
					<?php if (fw_get_db_settings_option($type) !== '') : ?>
					<li class="<?php echo esc_attr($type); ?>">
						<?php echo esc_attr(fw_get_db_settings_option($type)); ?>
					</li>
					<?php endif; ?>
				<?php
				endforeach;
			} else {
				foreach ($instance as $key => $value) :
					if ($key === 'src' || empty($value)):
						continue;
					endif;
					?>
					<li class="<?php echo esc_attr($key); ?>">
						<?php echo esc_attr($value); ?>
					</li>
				<?php
				endforeach;
			}
			?>
		</ul>
	</div>
	<?php echo $after_widget ?>
