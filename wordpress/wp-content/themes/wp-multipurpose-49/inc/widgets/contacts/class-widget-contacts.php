<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

class Widget_Contacts extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'description' => __( 'Contacts links', 'unyson' ) );

		parent::WP_Widget( false, __( 'Contacts', 'unyson' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$params = array();

		foreach ( $instance as $key => $value ) {
			$params[ $key ] = $value;
		}

		if (!isset($instance['src'])) {
			$params['src'] = false;
		}

		if (!isset($params['widget-title'])){
			$params['widget-title'] = '';
		}

		$title = $before_title . $params['widget-title'] . $after_title;
		unset( $params['widget-title'] );

		$filepath = dirname( __FILE__ ) . '/views/widget.php';

		$instance      = $params;
		$before_widget = str_replace( 'class="', 'class="widget_contacts_links ', $before_widget );

		if ( file_exists( $filepath ) ) {
			include( $filepath );
		}

		wp_enqueue_style(
			'fw-theme-contacts-widget',
			get_template_directory_uri() . '/inc/widgets/contacts/static/css/styles.css'
		);
	}

	function update( $new_instance, $old_instance ) {
		$instance = wp_parse_args( (array) $new_instance, $old_instance );

		return $instance;
	}

	function form( $instance ) {

		$titles = array(
			'widget-title' 	    => __( 'Contacts Title:', 'unyson' ),
			'phone' 			=> __( 'Phone:', 'unyson' ),
			'mail' 		        => __( 'E-Mail:', 'unyson' ),
			'url'     			=> __( 'Site URL:', 'unyson' ),
			'location' 			=> __( 'Address:', 'unyson' ),
			'src'               => __( 'Getting data from theme', 'unyson' ),
		);

		$instance = wp_parse_args( (array) $instance, $titles );

		foreach ( $instance as $key => $value ) {
			if (isset($titles[ $key ] )) {
				if ($key == 'src') { ?>
					<p>
						<label>
							<input type="hidden" name="<?php echo $this->get_field_name( $key ) ?>" value="">
							<input class="widefat widget_contacts_link widget_link_field"
						              name="<?php echo $this->get_field_name( $key ) ?>" type="checkbox"
						              <?php echo ($instance[ $key ] ? 'checked="checked"' : '') ?>
								value="on">
							<?php echo $titles[ $key ] ?>
						</label>
					</p>
				<?php } else { ?>

				<p>
					<label><?php echo $titles[ $key ] ?></label>
					<input class="widefat widget_contacts_link widget_link_field" <?php echo ($instance['src'] && $key !== 'widget-title' ) ? 'disabled' : '' ?>
					       name="<?php echo $this->get_field_name( $key ) ?>" type="text"
					       value="<?php echo ( $instance[ $key ] === $titles[ $key ] ) ? '' : $instance[ $key ]; ?>"/>
				</p>
			<?php }
			}
		}

	}
}
