<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * @var $number
 * @var $before_widget
 * @var $after_widget
 * @var $title
 * @var $flickr_id
 */

echo $before_widget;
echo $title;
?>
	<div class="wrap-portfolio">
		<?php
		$the_query = new WP_Query(array(
				'posts_per_page' => $number,
				'post_type' => 'fw-portfolio'
			)
		);

		while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

			<div class="widget-portfolio-item">
				<a href="<?php the_permalink(); ?>">
					<?php
					the_post_thumbnail('thumb', array('class' => 'widget-portfolio-img'));
					?>
				</a>
			</div>

		<?php endwhile;?>
	</div>
<?php echo $after_widget ?>