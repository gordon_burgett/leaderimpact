<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

class Widget_Portfolio extends WP_Widget {

	/**
	 * @internal
	 */
	function __construct() {
		$widget_ops = array( 'description' => '' );
		parent::WP_Widget( false, __( 'Portfolio', 'unyson' ), $widget_ops );
	}

	/**
	 * @param array $args
	 * @param array $instance
	 */
	function widget( $args, $instance ) {
		extract( $args );

		$title     = esc_attr( $instance['title'] );
		$number    = ( (int) ( esc_attr( $instance['number'] ) ) > 0 ) ? esc_attr( $instance['number'] ) : 9;
		$before_widget = str_replace( 'class="', 'class="widget_portfolio_image_gallery ', $before_widget );
		$title         = str_replace( 'class="', 'class="widget_portfolio_image_gallery ',
				$before_title ) . $title . $after_title;

		wp_enqueue_script(
			'fw-theme-portfolio-widget',
			get_template_directory_uri() . '/inc/widgets/portfolio/static/js/scripts.js',
			array( 'jquery' ),
			'1.0'
		);

		wp_enqueue_style(
			'fw-theme-portfolio-widget',
			get_template_directory_uri() . '/inc/widgets/portfolio/static/css/styles.css'
		);

		$filepath = dirname( __FILE__ ) . '/views/widget.php';

		if ( file_exists( $filepath ) ) {
			include( $filepath );
		}
	}

	function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'number' => '', 'title' => '' ) );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'unyson' ); ?> </label>
			<input type="text" name="<?php echo $this->get_field_name( 'title' ); ?>"
			       value="<?php echo esc_attr( $instance['title'] ); ?>" class="widefat"
			       id="<?php $this->get_field_id( 'title' ); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of images', 'unyson' ); ?>
				:</label>
			<input type="text" name="<?php echo $this->get_field_name( 'number' ); ?>"
			       value="<?php echo esc_attr( $instance['number'] ); ?>" class="widefat"
			       id="<?php echo $this->get_field_id( 'number' ); ?>"/>
		</p>
	<?php
	}
}
