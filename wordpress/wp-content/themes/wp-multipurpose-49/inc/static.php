<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }
/**
 * Include static files: javascript and css
 */

if ( is_admin() ) {
	return;
}

/**
 * Enqueue scripts and styles for the front end.
 */

// Loading a Bootstrap Framework stylescheet.
wp_enqueue_style(
	'bootstrap',
	get_template_directory_uri() . '/css/bootstrap.min.css',
	array(),
	'1.0'
);

// Add Genericons font, used in the main stylesheet.
wp_enqueue_style(
	'genericons',
	get_template_directory_uri() . '/genericons/genericons.css',
	array(),
	'1.0'
);

// Load our main stylesheet.
wp_enqueue_style(
	'fw-theme-style',
	get_stylesheet_uri(),
	array( 'genericons' ),
	'1.0'
);

// Load the Internet Explorer specific stylesheet.
wp_enqueue_style(
	'fw-theme-ie',
	get_template_directory_uri() . '/css/ie.css',
	array( 'fw-theme-style', 'genericons' ),
	'1.0'
);
wp_style_add_data( 'fw-theme-ie', 'conditional', 'lt IE 9' );

if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}

if ( is_singular() && wp_attachment_is_image() ) {
	wp_enqueue_script(
		'fw-theme-keyboard-image-navigation',
		get_template_directory_uri() . '/js/keyboard-image-navigation.js',
		array( 'jquery' ),
		'1.0'
	);
}

wp_enqueue_script(
	'jquery-ui-tabs',
	get_template_directory_uri() . '/js/jquery-ui-1.10.4.custom.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_script(
	'fw-theme-script',
	get_template_directory_uri() . '/js/functions.js',
	array( 'jquery' ),
	'1.0',
	true
);

// Font Awesome stylesheet
wp_enqueue_style(
	'font-awesome',
	get_template_directory_uri() . '/css/font-awesome/css/font-awesome.min.css',
	array(),
	'1.0'
);

wp_enqueue_script(
	'jquery-custom-input',
	get_template_directory_uri() . '/js/jquery.customInput.js',
	array( 'jquery' ),
	'1.0',
	true
);

// FancyBox
wp_enqueue_style(
    'fancybox',
    get_template_directory_uri() . '/fancybox/jquery.fancybox-1.3.7.min.css',
    array(),
    '1.0'
);

wp_enqueue_script(
    'fancybox',
    get_template_directory_uri() . '/fancybox/jquery.fancybox-1.3.7.min.js',
    array( 'jquery' ),
    '1.0'
);

// Magnific Popup
wp_enqueue_style(
	'magnific-popup',
	get_template_directory_uri() . '/magnific-popup/magnific-popup.css',
	array(),
	'1.0'
);

wp_enqueue_script(
	'magnific-popup',
	get_template_directory_uri() . '/magnific-popup/magnific-popup.js',
	array( 'jquery' ),
	'1.0'
);

// Selectize
{
	wp_enqueue_style(
		'selectize-css',
		get_template_directory_uri() . '/css/selectize.css',
		array(),
		'1.0'
	);
	wp_enqueue_script(
		'selectize-js',
		get_template_directory_uri() . '/js/selectize.js',
		array( 'jquery' ),
		'1.0',
		true
	);
}

// fw-frontend-grid
wp_enqueue_style(
	'fw-frontend-grid',
	get_template_directory_uri() . '/css/fw-frontend-grid.css',
	array(),
	'1.0'
);


//jQuery Easing
wp_enqueue_script(
	'jquery-easing',
	get_template_directory_uri() . '/js/jquery.easing-1.3.pack.js',
	array( 'jquery' ),
	'1.0',
	true
);

//jQuery Throttle Debounce Plugin
wp_enqueue_script(
	'throttle-debounce',
	get_template_directory_uri() . '/js/jquery.ba-throttle-debounce.min.js',
	array( 'jquery' ),
	'1.0',
	true
);


//jQuery tabulous
wp_enqueue_script(
	'tabulous',
	get_template_directory_uri() . '/js/tabulous.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_style(
	'tabulous',
	get_template_directory_uri() . '/css/tabulous.css',
	array(),
	'1.0'
);

// Add Custom stylesheet.
wp_enqueue_style(
	'custom',
	get_template_directory_uri() . '/custom.css',
	array(),
	'1.0'
);