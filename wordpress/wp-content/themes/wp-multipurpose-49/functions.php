<?php
if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

if (defined('FW')):
	// the framework was already included in another place, so this version will be inactive/ignored
else:
	/** @internal */
	function _filter_fw_framework_plugin_directory_uri() {
		return get_template_directory_uri() . '/framework';
	}
	add_filter('fw_framework_directory_uri', '_filter_fw_framework_plugin_directory_uri');
	require dirname(__FILE__) .'/framework/bootstrap.php';

endif;

function add_templates() {
	if ( fw()->extensions->get( 'builder' ) && !fw_get_db_extension_data('builder', '') ) {
		fw_set_db_extension_data('builder', '', json_decode(file_get_contents(dirname(__FILE__) . '/inc/templates.json'), true));
	}
}

add_action('after_setup_theme', 'add_templates');


function _fw_theme_after_switch_theme() {

	if (strrpos($_SERVER['REQUEST_URI'], '/themes.php?activated=true')) {
		wp_redirect(admin_url('themes.php?nocycle&activated=true'));
	}
}

add_action('after_switch_theme', '_fw_theme_after_switch_theme');


/**
 * Theme Includes
 */
require_once get_template_directory() .'/inc/init.php';

/**
 * Theme layout
 */
function fw_theme_layout($layout = '') {
	if ($layout !== ''){
		return $layout;
	}
	else {
		return fw_get_db_settings_option('layout/value');
	}
}

/**
 * Theme background
 */
function fw_theme_background ($background_image = '', $background_color = '') {
	$background_image = fw_get_db_settings_option('layout/box/background-image/url');
	$background_color = fw_get_db_settings_option('layout/box/background-color');
	$background = 'style="background-image: url( ' . $background_image . ' ); background-color: ' . $background_color . '";';

	$layout = fw_theme_layout();

	if ($layout === "full-width") {
	  $background = "";
	}

	return $background;
}

/**
 * Site Name
 */
function fw_theme_site_name () {
	return fw_get_db_settings_option('site-name');
}

/**
 * Site Logo
 */
function fw_theme_site_logo () {
	$logo = fw_get_db_settings_option('logo/url');
	$site_name = fw_theme_site_name();
	if (isset($logo)){
		return '<img src="' . $logo . '" alt="' . $site_name . '">';
	}
	else{
		return '<h1>' . $site_name . '</h1>';
	}

}

/**
 * Social icons 
 */

function fw_theme_social($position) {
	$soc = fw_get_db_settings_option('social-multi');
	$show = fw_get_db_settings_option('social-toggle');
	$result = "";

	if ($show === "true") {
		if ($position === "header") {
			foreach ($soc as $name => $url) {
				if (strlen($url) > 0) {
					$result .= '<a class="item social" href="//' . $url . '"><i class="fa fa-lg pad-left fa-' . $name . '"></i></a>';
				}
			}
		}
	}
	if ($position === "footer") {
		foreach ($soc as $name => $url) {
			if (strlen($url) > 0) {
				$result .= '<a href="//' . $url . '"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-' . $name . ' fa-stack-1x fa-inverse dark-blue"></i></span></a>';
			}
		}
	}
	echo $result;
}

/**
 * Contacts 
 */

function fw_theme_contacts() {

	$types = array('phone', 'mail', 'url', 'address');
	$result = '';

	foreach ($types as $type){
		$show = fw_get_db_settings_option($type.'_show');
		if (($show === 'yes') && (fw_get_db_settings_option($type) !== '')) {
			$result .= '<span class="item"><i class="fa fa-lg ' . fw_get_db_settings_option($type . '_icon') . '"></i>'  . fw_get_db_settings_option($type) . '</span>';
		}
		else{
			$result .= '';
		}

	}

	return $result;
}

/**
 * Top Bar Show/Hide
 */

function fw_theme_topbar() {
	$show = fw_get_db_settings_option('top_bar_show');
	$result = ($show == 'yes') ? true : false;
	return $result;
}

function fw_theme_topbar_styles(){
	$bg = fw_get_db_settings_option('top_bar_bg');
	$text = fw_get_db_settings_option('top_bar_color');
	return '.top-bar{background-color: ' . $bg . '; color:' . $text .	'}' .
	'.top-bar a{color:' . $text . '}' .
	'.top-bar .social .item, .top-bar .contacts .item {color:' . $text . '}';
}

/**
 * Sidebar position
 */

function sidebar_curr_position() {
	if (function_exists('fw_ext_sidebars_get_current_position')) {
		$current_position = fw_ext_sidebars_get_current_position();
		return $current_position;
	}
}

function with_sidebar() {
	$position = sidebar_curr_position();
	if ($position === 'left' || $position === 'right'){
		echo 'withsidebar ' . $position;
	}
}


/**
 * Helper for user's dimension input
 * @param string $value
 * @return string
 */

function fw_theme_normalize_css_size($value) {
	$value = str_replace(' ', '', $value);
	return is_numeric($value) ? $value . 'px' : $value;
}


function custom_thumbnail_size(){
	add_image_size( 'thumbnail', 150, 150, false);
}

add_action( 'init', 'custom_thumbnail_size');

function add_latest_posts_image(){
	add_image_size( 'latest-posts-image', 350, 300, true );
}

add_action( 'init', 'add_latest_posts_image');

function add_latest_link_posts_image(){
	add_image_size( 'link-posts-image', 180, 180, false );
}

add_action( 'init', 'add_latest_link_posts_image');


function add_portfolio_preview(){
	add_image_size( 'portfolio-preview', 270, 200, true );
}

add_action( 'init', 'add_portfolio_preview');


function add_avatar(){
	add_image_size( 'avatar', 100, 100, true );
}

add_action( 'init', 'add_avatar', 20 );

function get_post_link_url() {
	preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $url);
	if ( !$url ) {
		return false;
	}
	return esc_url_raw( $url[1] );
}

function new_excerpt_more( $more ) {
	return '...<p><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'unyson') . ' <i class="fa fa-chevron-right"></i></a></p>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_action( 'admin_head', 'fix_upload_image_size' );

function fix_upload_image_size(){
	echo '<style>.fw-option-type-upload.images-only .thumb {width: auto !important; height: auto;}'.
		'.fw-option-type-upload.images-only .thumb img {width: 100% !important; height: auto !important}' .
		'.fw-option-type-upload.images-only .thumb .no-image-img {width: auto !important; height: 100% !important}' .
		'.fw-backend-option-desc{padding-bottom: 20px;}' .
		'.fw-option-type-style .style-preview {left: auto !important; right: 30px !important;}' .
		'@media {}' .
		'</style>';
	;
}

// This theme support WooCommerce
add_theme_support( 'woocommerce' );

function parallax_slider_static() {
	$uri = fw_get_template_customizations_directory_uri('/extensions/media/extensions/slider/extensions/parallax/');

	wp_enqueue_script('jquery-ui-draggable');

	wp_register_style('slider-dnd-opt-css', $uri . '/admin-static/slider-dnd-opt.css', array(), '1.0' );
	wp_enqueue_style('slider-dnd-opt-css');
	wp_register_script('slider-dnd-opt-js', $uri . '/admin-static/slider-dnd-opt.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script('slider-dnd-opt-js');
}

add_action( 'admin_enqueue_scripts', 'parallax_slider_static' );