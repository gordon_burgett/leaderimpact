<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php
	$favicon = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('favicon') : '';
	if( !empty( $favicon ) ) :
		?>
		<link rel="icon" type="image/png" href="<?php echo $favicon['url'] ?>">
	<?php endif ?>

    <script>
      <?php
      // Fix ios8 responsive iframe
      // See http://stackoverflow.com/questions/23083462/how-to-get-an-iframe-to-be-responsive-in-ios-safari
      ?>
      if (/(iphone|ipod|ipad)/i.test(navigator.userAgent)) {
        document.addEventListener("DOMContentLoaded", function() {
          document.body.style.width = '1px';
          document.body.style.minWidth = '100%';
        });
      }
    </script>
	<link href='http://fonts.googleapis.com/css?family=Raleway:500,600,400' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
</head>
<?php
  $sidebar_pisition = sidebar_curr_position();
?>
<body <?php body_class(); ?> <?php echo fw_theme_background(); ?>>
<div id="page" class="hfeed site ">
  <?php if(fw_theme_topbar()) : ?>
    <?php if(!fw_theme_contacts() && !has_nav_menu('top_bar_menu') && (fw_get_db_settings_option('social-toggle') === 'false')):
      return;
    endif; ?>

    <?php
      $topBarItems = 0;
      if (fw_theme_contacts()) {
        $topBarItems += 1;
      }
      if (has_nav_menu('top_bar_menu')) {
        $topBarItems += 1;
      }
      if (fw_get_db_settings_option('social-toggle') === 'true') {
        $topBarItems += 1;
      }

      if ($topBarItems == 1){
        $col = 'col-md-12';
      } elseif ($topBarItems == 2) {
        $col = 'col-md-6';
      } elseif ($topBarItems == 3) {
        $col = 'col-md-4';
      }

    ?>
    <style>
    <?php echo fw_theme_topbar_styles(); ?>
    </style>
      <div class="top-bar">
        <div class="container">
          <div class="row">
          <?php if (fw_theme_contacts()) : ?>
            <div class="col-xs-12 <?php echo $col; ?> contacts col">
              <?php echo fw_theme_contacts(); ?>
            </div>
          <?php endif; ?>
          <?php if (has_nav_menu('top_bar_menu')) : ?>
            <div class="col-xs-12 <?php echo $col; ?> col">
              <?php wp_nav_menu( array( 'theme_location' => 'top_bar_menu', 'container_class' => 'top-bar-menu', 'depth' => 1) ); ?>
            </div>
          <?php endif; ?>
          <?php if (fw_get_db_settings_option('social-toggle') === 'true') : ?>
            <div class="col-xs-12 <?php echo $col; ?> social col">
              <?php fw_theme_social("header"); ?>
            </div>
          <?php endif; ?>
          </div>
        </div>
      </div>
  <?php endif; ?>

    <div class="header" id="masthead">
      <div class="header-container container">
        <div class="isMobile"></div>
          <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'nav-menu-mobile' ) ); ?>
        <a class="menu-button" href="#"><i class="fa fa-bars"></i></a>
        <div class="logo">
          <a href="<?php echo  get_site_url(); ?>"><?php echo fw_theme_site_logo(); ?></a>
        </div>
        <div class="primary-navigation">
          <?php if (has_nav_menu('primary')) : ?>
          <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'nav-menu' ) ); ?>
          <?php endif; ?>
          <ul class="nav-menu-hiddens"></ul>
        </div>
      </div>
    </div>

  <div class="main-container <?php echo ($sidebar_pisition != '') ? fw_theme_layout('box') : fw_theme_layout(); ?>">

