*** WP-Multipurpose Version Changelog ***

/*-------------------------------------------------------*/
// Version 1.7.0
/*-------------------------------------------------------*/
 * Added custom.css
 * Minor bug-fixes

/*-------------------------------------------------------*/
// Version 1.6.0
/*-------------------------------------------------------*/
 * Added Styling Front-End panel.
 * Fixed notifications on column and section shortcodes.
 * Fixed content install notifications.

/*-------------------------------------------------------*/
// Version 1.5.0
/*-------------------------------------------------------*/
 * Added top-bar configurator.
