<?php
/**
 * The Footer Sidebar
 */

if ( ! is_active_sidebar( 'sidebar-footer' ) ) {
	return;
}
?>

<div id="secondary" class="footer-sidebar widget-area section dark" role="complementary">
  <div class="container">
    <div class="row">
      <?php dynamic_sidebar( 'sidebar-footer' ); ?>
    </div>
  </div>
</div><!-- #footer-sidebar -->