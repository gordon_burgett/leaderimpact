<?php
/**
 * The template for displaying posts in the Image post format
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class('fw-container'); ?>>

	<header class="entry-header">

		<?php
			if ( is_single() ) :
				the_title( '<h3 class="entry-title">', '</h3>' );
			else :
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			endif;
		?>
		<div class="entry-meta">
			<span class="post-format">
				<a class="entry-format" href="<?php echo esc_url( get_post_format_link( 'image' ) ); ?>" title="<?php echo get_post_format_string( 'image' );?>"><span class="fa fa-image"></span></a>
			</span>
			<?php fw_theme_posted_on(); ?>

			<?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
				<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'unyson' ), __( '1 Comment', 'unyson' ), __( '% Comments', 'unyson' ) ); ?></span>
			<?php endif; ?>

			<?php edit_post_link( __( 'Edit', 'unyson' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-meta -->


		<div class="thumb-container">
			<?php
			if ( has_post_thumbnail() ) :
				if ( is_single() ) :
					the_post_thumbnail('large', array('class' => 'thumb_sh'));
				else:
			?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php
					the_post_thumbnail('large', array('class' => 'thumb'));
				?>
				</a>
			<?php endif; ?>
			<?php endif; ?>
		</div>


	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php
			if ( is_single() ) :
				the_content();
			endif;
		?>

	</div><!-- .entry-content -->

	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->
