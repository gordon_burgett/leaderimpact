<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
		'title'   => __( 'General', 'unyson' ),
		'type'    => 'tab',
		'options' => array(
			'logo'             => array(
				'label' => __( 'Logo', 'unyson' ),
				'desc'  => __( 'Choose your website logo', 'unyson' ),
				'type'  => 'upload'
				),
			'site-name'    => array(
				'label' => __( 'Site Name', 'unyson' ),
				'desc'  => __( 'Write your website name', 'unyson' ),
				'type'  => 'text',
				'value' => get_bloginfo( 'name' )
				),
			'favicon' => array(
				'label' => __( 'Favicon', 'unyson' ),
				'desc'  => __( 'Upload a favicon image (PNG, ICO. JPEG is not recommended)', 'unyson' ),
				'type'  => 'upload'
				),
			'layout'       => array(
				'type'         => 'multi-picker',
				'label'        => false,
				'desc'         => false,
				'picker'       => array(
					'value' => array(
						'label'        => __( 'Layout', 'unyson' ),
						'type'         => 'switch',
						'right-choice' => array(
							'value' => 'box',
							'label' => __( 'Box', 'unyson' )
							),
						'left-choice'  => array(
							'value' => 'full-width',
							'label' => __( 'full-width', 'unyson' )
							),
						'value'        => 'yes',
						),
				),
				'choices'      => array(
					'box'  => array(
						'background-image'             => array(
							'label' => __( 'Background Image', 'unyson' ),
							'desc'  => __( 'Choose your website background image', 'unyson' ),
							'type'  => 'upload'
							),
						'background-color'              => array(
							'label' => __( 'Background Color', 'unyson' ),
							'type'  => 'color-picker',
							'value' => '',
							'desc'  => __( '', 'unyson' ),
							),
						),
					'fullscreen' => array(),
				),
			),
			'top_bar_show'  => array(
				'label'        => __( 'Top Bar Show\Hide', 'unyson' ),
				'type'         => 'switch',
				'right-choice' => array(
					'value' => 'yes',
					'label' => __( 'Yes', 'unyson' )
				),
				'left-choice'  => array(
					'value' => 'no',
					'label' => __( 'No', 'unyson' )
				),
				'value'        => 'yes',
			),
			'top_bar_bg' => array(
				'type'  => 'color-picker',
				'value' => '#363f48',
				'label' => __('Top Bar Background', 'fw')
			),
			'top_bar_color' => array(
				'type'  => 'color-picker',
				'value' => '#a4acb5',
				'label' => __('Top Bar Text Color', 'fw')
			),

		)
	)
);
