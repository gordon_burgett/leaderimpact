<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'social' => array(
		'title'   => __( 'Social', 'unyson' ),
		'type'    => 'tab',
		'options' => array(
			'social-toggle' => array( 
		    'type'  => 'switch',
		    'value' => 'hello',
		    'label' => __('Show/hide social media icons on the Top Bar', 'unyson'),
		    'left-choice' => array(
		        'value' => 'true',
		        'label' => __('Show', 'unyson'),
		    ),
		    'right-choice' => array(
		        'value' => 'false',
		        'label' => __('Hide', 'unyson'),
		    ),
			),
			'social-multi' => array(
				'label'         => false,
				'type'          => 'multi',
				'value'         => array(),
				'inner-options' => array(
					
					'facebook'    => array(
						'label' => __( 'Facebook', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'twitter'    => array(
						'label' => __( 'Twitter', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'google'    => array(
						'label' => __( 'Google Plus', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'linkedin'    => array(
						'label' => __( 'LinkedIn', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'youtube'    => array(
						'label' => __( 'YouTube', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'instagram'    => array(
						'label' => __( 'Instagram', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'pinterest'    => array(
						'label' => __( 'Pinterest', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'flickr'    => array(
						'label' => __( 'Flickr', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
					'rss'    => array(
						'label' => __( 'RSS', 'unyson' ),
						'type'  => 'text',
						'value' => ''
					),
				),
			),
		)
	)
);