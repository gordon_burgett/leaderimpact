<?php if ( ! defined( 'FW' ) ) {
  die( 'Forbidden' );
}

$options = array(

  'contacts' => array(
    'title'   => __( 'Contacts', 'unyson' ),
    'type' => 'tab',
    'options' => array(

      'phone_box' => array(
        'type' => 'box',
        'title' => 'Phone',
        'options' => array(
          'phone'    => array(
            'label' => __( 'Phone Number', 'unyson' ),
            'desc'  => __( '', 'unyson' ),
            'type'  => 'text',
            'value' => ''
          ),
          'phone_icon' => array(
            'label' => __( 'Icon', 'unyson' ),
            'type'  => 'icon',
            'value' => 'fa fa-phone',
            ),
          'phone_show' => array(
            'label'        => __( 'Show on Top Bar', 'unyson' ),
            'type'         => 'switch',
            'right-choice' => array(
              'value' => 'yes',
              'label' => __( 'Yes', 'unyson' )
            ),
            'left-choice'  => array(
              'value' => 'no',
              'label' => __( 'No', 'unyson' )
            ),
            'value'        => 'yes',
          ),
        ),
      ),

      'mail_box' => array(
        'type' => 'box',
        'title' => 'E-mail',
        'options' => array(
          'mail'    => array(
            'label' => __( 'E-mail', 'unyson' ),
            'desc'  => __( '', 'unyson' ),
            'type'  => 'text',
            'value' => ''
          ),
          'mail_icon' => array(
            'label' => __( 'Icon', 'unyson' ),
            'type'  => 'icon',
            'value' => 'fa fa-envelope',
          ),
          'mail_show'                    => array(
            'label'        => __( 'Show on Top Bar', 'unyson' ),
            'type'         => 'switch',
            'right-choice' => array(
              'value' => 'yes',
              'label' => __( 'Yes', 'unyson' )
            ),
            'left-choice'  => array(
              'value' => 'no',
              'label' => __( 'No', 'unyson' )
            ),
            'value'        => 'yes',
          ),
        ),
      ),

      'url_box' => array(
        'type' => 'box',
        'title' => 'Site URL',
        'options' => array(
          'url'    => array(
            'label' => __( 'Site URL', 'unyson' ),
            'desc'  => __( '', 'unyson' ),
            'type'  => 'text',
            'value' => ''
          ),
          'url_icon' => array(
            'label' => __( 'Icon', 'unyson' ),
            'type'  => 'icon',
            'value' => 'fa fa-globe',
          ),
          'url_show' => array(
            'type'  => 'hidden',
            'value' => 'no'
          )
        ),
      ),

      'location_box' => array(
        'type' => 'box',
        'title' => 'Address',
        'options' => array(

          'location'    => array(
            'label' => __( 'Address', 'unyson' ),
            'desc'  => __( '', 'unyson' ),
            'type'  => 'text',
            'value' => ''
          ),
          'location_icon' => array(
            'label' => __( 'Icon', 'unyson' ),
            'type'  => 'icon',
            'value' => 'fa fa-map-marker',
          ),
          'location_show' => array(
            'type'  => 'hidden',
            'value' => 'no'
          )
        ),
      )
    )
  )
);


