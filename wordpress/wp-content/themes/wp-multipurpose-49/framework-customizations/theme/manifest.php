<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['id'] = 'multipurpose';

$manifest['name'] = __('WP-multipurpose', 'multipurpose');

$manifest['uri']          = 'http://www.solostream.com';
$manifest['description']  = __('SoloStream Premium WordPress Theme', 'unyson');
$manifest['version']      = '1.6.49';
$manifest['author']       = 'SoloStream, LLC';
$manifest['author_uri']   = 'www.solostream.com';

$manifest['requirements'] = array(
    'wordpress' => array(
        'min_version' => '4.0',
        /*'max_version' => '4.99.9'*/
    ),
//    'framework' => array(
//        'min_version' => '2.1.25',
//        /*'min_version' => '1.0.0',
//        'max_version' => '1.99.9'*/
//    ),
//    'extensions' => array(
//        'page-builder' => array(),
//        'slider' => array(),
//        'portfolio' => array(),
//        'megamenu' => array(),
//        'sidebars' => array(),
//        'backup' => array()
//    )
);

$manifest['supported_extensions'] = array(
	'builder' => array(),
	'slider' => array(),
	'portfolio' => array(),
	'megamenu' => array(),
	'styling' => array(),
	'sidebars' => array(),
	'backup' => array()
);
