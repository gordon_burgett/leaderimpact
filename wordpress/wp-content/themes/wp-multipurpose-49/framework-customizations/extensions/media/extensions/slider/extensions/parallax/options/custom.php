<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'layers' => array(
		'type'  => 'addable-box',
		'value' => array(
			array(
//				'option_1' => 'value 1',
			)
		),
		'label' => __('Layers', 'unyson'),
		'template' => 'Layer',
		'box-options' => array(
			'background' => array(
				'type'  => 'upload',
				'label'   => __( 'Background', 'unyson' ),
				'images_only' => true
			),
			'type'  => array(
				'type'         => 'multi-picker',
				'label'        => false,
				'desc'         => false,
				'picker'       => array(
					'val' => array(
						'label'   => __( 'Content', 'unyson' ),
						'type'    => 'select',
						'choices' => array(
							'image'		=> __( 'Image', 'unyson' ),
							'text' 		=> __( 'Text', 'unyson' ),
							'button'	=> __( 'Button', 'unyson' ),
						)
					)
				),
				'choices'      => array(
					'image'  => array(
						'content' => array(
							'type'  => 'upload',
							'label'   => __( 'Image', 'unyson' ),
							'images_only' => true,
						)
					),
					'text' => array(
						'content' => array(
							'type'  => 'textarea',
							'label'   => __( 'Text', 'unyson' ),
						),
						'size'	=> array(
							'type'  => 'select',
							'value' => '16',
							'label' => __('Font Size', 'fw'),
							'choices' => array(
								'9' => __('9px', 'fw'),
								'10' => __('10px', 'fw'),
								'11' => __('11px', 'fw'),
								'12' => __('12px', 'fw'),
								'13' => __('13px', 'fw'),
								'14' => __('14px', 'fw'),
								'15' => __('15px', 'fw'),
								'16' => __('16px', 'fw'),
								'17' => __('17px', 'fw'),
								'18' => __('18px', 'fw'),
								'19' => __('19px', 'fw'),
								'20' => __('20px', 'fw'),
								'21' => __('21px', 'fw'),
								'22' => __('22px', 'fw'),
								'23' => __('23px', 'fw'),
								'24' => __('24px', 'fw'),
								'25' => __('25px', 'fw'),
								'26' => __('26px', 'fw'),
								'27' => __('27px', 'fw'),
								'28' => __('28px', 'fw'),
								'29' => __('29px', 'fw'),
								'30' => __('30px', 'fw'),
								'31' => __('31px', 'fw'),
								'32' => __('32px', 'fw'),
								'33' => __('33px', 'fw'),
								'34' => __('34px', 'fw'),
								'35' => __('35px', 'fw'),
								'36' => __('36px', 'fw'),
								'37' => __('37px', 'fw'),
								'38' => __('38px', 'fw'),
								'39' => __('39px', 'fw'),
								'40' => __('40px', 'fw'),
								'41' => __('41px', 'fw'),
								'42' => __('42px', 'fw'),
								'43' => __('43px', 'fw'),
								'44' => __('44px', 'fw'),
								'45' => __('45px', 'fw'),
								'46' => __('46px', 'fw'),
								'47' => __('47px', 'fw'),
								'48' => __('48px', 'fw'),
								'49' => __('49px', 'fw'),
								'50' => __('50px', 'fw'),
								'51' => __('51px', 'fw'),
								'52' => __('52px', 'fw'),
								'53' => __('53px', 'fw'),
								'54' => __('54px', 'fw'),
								'55' => __('55px', 'fw'),
								'56' => __('56px', 'fw'),
								'57' => __('57px', 'fw'),
								'58' => __('58px', 'fw'),
								'59' => __('59px', 'fw'),
								'60' => __('60px', 'fw'),
								'61' => __('61px', 'fw'),
								'62' => __('62px', 'fw'),
								'63' => __('63px', 'fw'),
								'64' => __('64px', 'fw'),
								'65' => __('65px', 'fw'),
								'66' => __('66px', 'fw'),
								'67' => __('67px', 'fw'),
								'68' => __('68px', 'fw'),
								'69' => __('69px', 'fw'),
								'70' => __('70px', 'fw'),

							),
							'no-validate' => false,
						),
						'color' => array(
							'type'  => 'color-picker',
							'value' => '#000000',
							'label' => __('Color', 'fw')
						)
					),
					'button' => array(
						'content' => array(
							'type'  => 'textarea',
							'label'   => __( 'Text', 'unyson' ),
						),
						'link' => array(
							'type'  => 'text',
							'label'   => __( 'Link', 'unyson' ),
						),
						'color' 		=> array(
							'type'  => 'color-picker',
							'value' => '#4c4c4c',
							'label' => __('Text Color', 'fw')),
						'bg_color'	=> array(
							'type'  => 'color-picker',
							'value' => '#eb4b13',
							'label' => __('Background', 'fw'))
					)
				),
				'show_borders' => false,
			),

			'pos-picker' => array(
				'type'	=> 'group',
				'attr'	=> array( 'class' => 'pos-picker' ),
				'options' => array(
					'ui-picker'       => array(
						'type'  => 'html',
						'attr'  => array( 'class' => 'ui-picker ' ),
						'label' => __('Position', 'fw'),
						'html'  => '<div class="ui-picker-container"><div class="ui-picker-el"></div></div>',
					),

					'posx' => array(
						'type'  => 'text',
						'attr'  => array( 'class' => 'ui-picker-val-x' ),
						'value'	=> '50%',
						'label' => __('Horizontal', 'fw'),
					),

					'posy' => array(
						'type'  => 'text',
						'attr'  => array( 'class' => 'ui-picker-val-y' ),
						'value'	=> '50%',
						'label' => __('Vertical', 'fw'),
					),
				)
			),

			'frames' => array(
				'type' => 'addable-popup',
				'label' => __('Frames', 'unyson'),
				'template' => 'Frame',
				'popup-title' => null,
				'size' => 'large', // small, medium, large
				'popup-options' => array(
					'pos-picker' => array(
						'type'	=> 'group',
						'attr'	=> array( 'class' => 'pos-picker' ),
						'options' => array(
							'ui-picker'       => array(
								'type'  => 'html',
								'attr'  => array( 'class' => 'ui-picker ' ),
								'label' => __('Position', 'fw'),
								'html'  => '<div class="ui-picker-container"><div class="ui-picker-el"></div></div>',
							),

							'posx' => array(
								'type'  => 'text',
								'attr'  => array( 'class' => 'ui-picker-val-x' ),
								'value'	=> '50%',
								'label' => __('Horizontal', 'fw'),
							),

							'posy' => array(
								'type'  => 'text',
								'attr'  => array( 'class' => 'ui-picker-val-y' ),
								'value'	=> '50%',
								'label' => __('Vertical', 'fw'),
							),
						)
					),

					'delay' => array(
						'type'  => 'text',
						'label' => __('Delay, ms', 'unyson'),
						'value'	=> '0'
					),
					'speed' => array(
						'type'  => 'text',
						'label' => __('Speed, ms', 'unyson'),
						'value'	=> '1000'
					),
					'easing' =>	array(
						'type'  => 'image-picker',
						'value' => 'swing',
						'label' => __('Easing', 'fw'),
						'choices' => array(

							'swing'							=> get_template_directory_uri() .'/images/easing/swing.png',
							'easeInQuad'				=> get_template_directory_uri() .'/images/easing/inquad.png',
							'easeOutQuad'				=> get_template_directory_uri() .'/images/easing/outquad.png',
							'easeInOutQuad'			=> get_template_directory_uri() .'/images/easing/inoutquad.png',
							'easeInCubic'				=> get_template_directory_uri() .'/images/easing/incubic.png',
							'easeOutCubic'			=> get_template_directory_uri() .'/images/easing/outcubic.png',
							'easeInOutCubic'		=> get_template_directory_uri() .'/images/easing/inoutcubic.png',
							'easeInQuart'				=> get_template_directory_uri() .'/images/easing/inquart.png',
							'easeOutQuart'			=> get_template_directory_uri() .'/images/easing/outquart.png',
							'easeInOutQuart'		=> get_template_directory_uri() .'/images/easing/inoutquart.png',
							'easeInQuint'				=> get_template_directory_uri() .'/images/easing/inquint.png',
							'easeOutQuint'			=> get_template_directory_uri() .'/images/easing/outquint.png',
							'easeInOutQuint'		=> get_template_directory_uri() .'/images/easing/inoutquint.png',
							'easeInSine'				=> get_template_directory_uri() .'/images/easing/insine.png',
							'easeOutSine'				=> get_template_directory_uri() .'/images/easing/outsine.png',
							'easeInOutSine'			=> get_template_directory_uri() .'/images/easing/inoutsine.png',
							'easeInExpo'				=> get_template_directory_uri() .'/images/easing/inexpo.png',
							'easeOutExpo'				=> get_template_directory_uri() .'/images/easing/outexpo.png',
							'easeInOutExpo'			=> get_template_directory_uri() .'/images/easing/inoutexpo.png',
							'easeInCirc'				=> get_template_directory_uri() .'/images/easing/incirc.png',
							'easeOutCirc'				=> get_template_directory_uri() .'/images/easing/outcirc.png',
							'easeInOutCirc'			=> get_template_directory_uri() .'/images/easing/inoutcirc.png',
							'easeInElastic'			=> get_template_directory_uri() .'/images/easing/inelastic.png',
							'easeOutElastic'		=> get_template_directory_uri() .'/images/easing/outelastic.png',
							'easeInOutElastic'	=> get_template_directory_uri() .'/images/easing/inoutelastic.png',
							'easeInBack'				=> get_template_directory_uri() .'/images/easing/inback.png',
							'easeOutBack'				=> get_template_directory_uri() .'/images/easing/outback.png',
							'easeInOutBack'			=> get_template_directory_uri() .'/images/easing/inoutback.png',
							'easeInBounce'			=> get_template_directory_uri() .'/images/easing/inbounce.png',
							'easeOutBounce'			=> get_template_directory_uri() .'/images/easing/outbounce.png',
							'easeInOutBounce'		=> get_template_directory_uri() .'/images/easing/inoutbounce.png',
						),
					)
				),
			),
			'loop' => array(
				'type'  => 'checkbox',
				'value' => false,
				'label' => __('Loop', 'unyson'),
				'text'  => __('Yes', 'unyson'),
			),
			'delayRepeat' => array(
				'type'  => 'checkbox',
				'value' => false,
				'label' => __('Dont Delay on Repeat', 'unyson'),
				'text'  => __('Yes', 'unyson'),
			)
		),
		'limit' => 10,
	)
);

