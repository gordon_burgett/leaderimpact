<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

if ( is_admin() ) {
    $uri = fw_get_template_customizations_directory_uri('/extensions/media/extensions/slider/extensions/parallax');
    wp_enqueue_script(
        'slider-dnd-opt',
        $uri . '/static/js/slider-dnd-opt.js',
        array( 'jquery' ),
        '1.0',
        true
    );
    wp_enqueue_style(
        'slider-dnd-opt',
        $uri . '/static/css/slider-dnd-opt.css',
        array(),
        '1.0'
    );
}



