<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$options = array(
	'title' 	=> array(
		'type'  => 'typography',
		'value' => array(
			'size'   => 38,
			'color'  => '#000000'
		),
		'components' => array(
			'family' => false,
			'size'   => true,
			'color'  => true,
			'style'	 => false,
		),
		'label' => __('Title', 'unyson'),
	),
	'desc' 	=> array(
		'type'  => 'typography',
		'value' => array(
			'size'   => 20,
			'color'  => '#999999'
		),
		'components' => array(
			'family' => false,
			'size'   => true,
			'color'  => true,
			'style'	 => false,
		),
		'label' => __('Description', 'unyson'),
	),
	'buttons' => array(
		'attr'  => array( 'class' => 'admin-slider-buttons'),
		'type' => 'addable-popup',
		'label' => __('Buttons', 'unyson'),
		'desc'  => __('Place buttons to your own slider! <br> Available only with fullwidth and paper slider.', 'unyson'),
		'template' => 'Button',
		'popup-options' => array(
			'label'  => array(
				'label' => __( 'Button Label', 'unyson' ),
				'desc'  => __( 'This is the text that appears on your button', 'unyson' ),
				'type'  => 'text',
				'value' => 'Submit'
			),
			'link'   => array(
				'label' => __( 'Button Link', 'unyson' ),
				'desc'  => __( 'Where should your button link to', 'unyson' ),
				'type'  => 'text',
				'value' => '#'
			),
			'target' => array(
				'type'  => 'switch',
				'label'   => __( 'Open Link in New Window', 'unyson' ),
				'desc'    => __( 'Select here if you want to open the linked page in a new window', 'unyson' ),
				'right-choice' => array(
					'value' => '_blank',
					'label' => __('Yes', 'unyson'),
				),
				'left-choice' => array(
					'value' => '_self',
					'label' => __('No', 'unyson'),
				),
			),
			'color'  => array(
				'type'  => 'color-picker',
				'value' => '#4eaade',
				'label' => __('Color', 'unyson')
			),

			'corner' => array(
				'type'  => 'radio',
				'value' => '',
				'label' => __('Corners', 'unyson'),
				'choices' => array(
					'' => __('Normal', 'unyson'),
					'round' => __('Round', 'unyson'),
					'square' => __('Square', 'unyson'),
				),
			),
			'type' => array(
				'type'  => 'switch',
				'value' => '',
				'label' => __('Inverted', 'unyson'),
				'left-choice' => array(
					'value' => 'inverted',
					'label' => __('True', 'unyson'),
				),
				'right-choice' => array(
					'value' => '',
					'label' => __('False', 'unyson'),
				),
			),
			'mode' => array(
				'type'  => 'switch',
				'value' => '',
				'label' => __('Disabled', 'unyson'),
				'left-choice' => array(
					'value' => 'disabled',
					'label' => __('True', 'unyson'),
				),
				'right-choice' => array(
					'value' => '',
					'label' => __('False', 'unyson'),
				),
			)
		),
	),
);

