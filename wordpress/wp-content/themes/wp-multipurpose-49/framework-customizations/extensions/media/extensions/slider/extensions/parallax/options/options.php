<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'autoslide' => array(
		'label' => __( 'Automatic cycling', 'unyson' ),
		'type'  => 'checkbox',
		'value' => '',
	),
	'delay' => array(
		'label' => __( 'Delay between cycles in milliseconds', 'unyson' ),
		'type' => 'text',
		'value' => 5000,
	),
);
