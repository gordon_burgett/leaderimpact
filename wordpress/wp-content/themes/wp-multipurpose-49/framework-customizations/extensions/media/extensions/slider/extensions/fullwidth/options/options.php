<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'autoslide' => array(
		'label' => __( 'Automatic cycling', 'fw' ),
		'type'  => 'checkbox',
		'value' => '',
	),
	'delay' => array(
		'label' => __( 'Delay between cycles in milliseconds', 'fw' ),
		'type' => 'text',
		'value' => 5000,
	),
);
