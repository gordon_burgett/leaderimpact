<?php
if (!defined('FW')) {
	die('Forbidden');
}
$cycling = '';
$id = uniqid('parallax-');

if ($data['settings']['extra']['autoslide']) {
	$cycling = 'data-cycling="slides" data-cyclingDelay="' . $data['settings']['extra']['delay'] . '"';
}

$i = 0;

$width = $dimensions['width'] ? fw_theme_normalize_css_size($dimensions['width']) : '100%';
$height = $dimensions['height'] ? fw_theme_normalize_css_size($dimensions['height']) : '600px';

$dim = 'style="max-width:' . $width . '; max-height:' . $height .';"';

?>
<div class="mightyslider_modern_skin mightyslider_parallax" <?php echo $dim; ?> id="<?php echo $id; ?>">
	<div class="frame" <?php echo $cycling; ?> <?php echo $dim; ?> data-mightyslider="width: <?php echo strpos($width, '%') ? '1200' : (int)$width; ?>, height: <?php echo (int)$height; ?>">
		<div class="slide_element">
				<?php
				foreach ($data['slides'] as $slide) {
					$slideData = array(
						'cover' => $slide['src'],
					);
				?>
			<div class="slide" data-mightyslider="<?php echo htmlspecialchars(trim(json_encode($slideData), '{}[]'), ENT_QUOTES, 'UTF-8'); ?>">
				<?php
					foreach ($slide['extra']['layers'] as $layer) {
						$id = 'layer_' . $i;
						$i++;

						$layerOptions = array(
							'background' => isset($layer['background']['url']) ? $layer['background']['url'] : '',
							'loop' => false,
							'dontDelayOnRepeat' =>($layer['delayRepeat'] ? true : false),
						);

						$layerOptionsJson = trim(json_encode($layerOptions), '[]{}');
						$layerOptionsJsonFormat = str_replace('"', '\'', $layerOptionsJson);


						$animationOptions = array();

						if ($layer['frames']) {
							foreach ($layer['frames'] as $frame) {

								$frameOptions = array(
									'delay' => (int)$frame['delay'],
									'speed' => (int)$frame['speed'],
									'easing' => $frame['easing'],
								);

								$style = array();

								$style['top'] = $frame['posy'];
								$style['left'] = $frame['posx'];

								$frameOptions['style'] = $style;
								array_push($animationOptions, $frameOptions);
							}
						}
						$animationOptionsJson = trim(json_encode($animationOptions), '[]');
						$animationOptionsJsonFormat = str_replace('"', '\'', $animationOptionsJson);

						if ($layer['type']['val'] === 'text') { ?>

							<style>
								<?php echo '#' . $id; ?>
								{
									font-size:<?php echo $layer['type']['text']['size'] . 'px'; ?>;
									color:<?php echo $layer['type']['text']['color']; ?>;
									top:<?php echo $layer['posy']; ?>;
									left:<?php echo $layer['posx']; ?>;
								}
							</style>

						<?php
						}
						elseif ($layer['type']['val'] === 'button') { ?>

							<style>
								<?php echo '#' . $id; ?>
								{
									color:<?php echo $layer['type']['button']['color']; ?>;
									background:<?php echo $layer['type']['button']['bg_color']; ?>;
									top:<?php echo $layer['posy']; ?>;
									left:<?php echo $layer['posx']; ?>;
								}
							</style>

						<?php
						}
						elseif($layer['type']['val'] === 'button') { ?>

							<style>
								<?php echo '#' . $id; ?>
								{
									color:<?php echo $layer['type']['button']['color']; ?>;
									background:<?php echo $layer['type']['button']['bg_color']; ?>;
									top:<?php echo $layer['posy']; ?>;
									left:<?php echo $layer['posx']; ?>;
								}
							</style>

						<?php
						}

						?>
						<div id="<?php echo $id; ?>" class="mSCaption <?php if($layer['type']['val'] === 'button') echo 'slider-button'; ?>" data-mightyslider="<?php echo $layerOptionsJsonFormat; ?>"
								 data-msanimation="<?php echo $animationOptionsJsonFormat; ?>">
							<?php
								if ($layer['type']['val'] === 'text') {
									?>
									<span class="text"><?php echo $layer['type']['text']['content']; ?></span>
								<?php
								}
								elseif ($layer['type']['val'] === 'image') {
									if (isset($layer['type']['image']['content']['url'])) {
										echo '<img src="' . $layer['type']['image']['content']['url'] . '">';
									}
								}
								elseif ($layer['type']['val'] === 'button') { ?>
									<a href="<?php echo empty($layer['type']['button']['link']) ? '/' : $layer['type']['button']['link']; ?>"><?php echo $layer['type']['button']['content']; ?></a>
								<?php
								}
							?>
						</div>
					<?php	}	?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php
wp_enqueue_script(
	'mightyslider',
	get_template_directory_uri() . '/js/mightyslider.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_script(
	'tweenlite',
	get_template_directory_uri() . '/js/tweenlite.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_style(
	'mightyslider',
	get_template_directory_uri() . '/css/mightyslider/mightyslider.css',
	array(),
	'1.0'
);
?>

<?php
if (!empty($dimensions['header_slider'])){
	add_filter('body_class', 'setSliderHeader');
}
?>

<script>
	jQuery(document).ready(function($) {
		var isTouch = !!('ontouchstart' in window),
			clickEvent = isTouch ? 'tap' : 'click';

		var slides = [];

		(function(){
			var $carousel = $('#<?php echo $id; ?>'),
				$frame = $('.frame', $carousel);

			var sliderEl = $frame.mightySlider({
					speed: 500,
					easing: 'easeOutExpo',
					autoScale: 1,

					// Navigation options
					navigation: {
						slideSize: '100%',
						keyboardNavBy: 'slides'
					},

					// Pages
					pages: {
						activateOn: 'click'
					},

					// Commands
					commands: {
						buttons: 1,
						pages: 1
					},

					parallax: {
						x:  1,           // Move in X axis parallax layers. where: 1 = enable, 0 = disable.
						y:  1,             // Move in Y axis parallax layers. where: 1 = enable, 0 = disable.
						z:  1,                // Move in Z axis parallax layers. where: 1 = enable, 0 = disable.
						invertX:   1,         // Invert X axis movements. where: 1 = enable, 0 = disable.
						invertY:   1,         // Invert Y axis movements. where: 1 = enable, 0 = disable.
						revert:   1,          // Whether the layers should revert to theirs start position when mouse leaved the slider. where: 1 = enable, 0 = disable.
						revertDuration: 200  // The duration of the revert animation, in milliseconds.
					}


				},
				{
					active: function (name, index) {}
				});

			// Disable reset slides on change page visibility
			var slider = sliderEl.data('mightySlider');
			var visibilityEventName = (function() {
				var prefixes = ['', 'webkit', 'moz', 'ms', 'o'], prefix;
				while ((prefix = prefixes.pop()) != undefined) {
					if (typeof document[(prefix ? prefix + 'H': 'h') + 'idden'] === 'boolean') {
						return prefix + 'visibilitychange';
					}
				}
			})();
			$(document).unbind(visibilityEventName + '.' + slider.uniqId);
		})();
	});
</script>
