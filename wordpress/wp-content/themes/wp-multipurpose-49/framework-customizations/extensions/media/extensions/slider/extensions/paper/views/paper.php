<?php
if (!defined('FW')) {
	die('Forbidden');
}

$id = uniqid('paper-');

$cycling = '';
if ($data['settings']['extra']['autoslide']) {
	$cycling = 'data-cycling="slides" data-cyclingDelay="' . $data['settings']['extra']['delay'] . '"';
}

?>

<div class="mightySlider mightyslider_paper_skin mightySlider_paper" id="<?php echo $id; ?>">   <!-- PARENT -->
	<div class="frame" <?php echo $cycling?>>    <!-- FRAME -->
		<ul class="slideelement">   <!-- SLIDEELEMENT -->
			<?php

			foreach ($data['slides'] as $slide) {

				$slideData = array(
					'cover' => $slide['src'],
					'link'  => array( 'url' => $slide['src']),
					'title' => $slide['title'],
					'desc'  => $slide['desc'],
				);

				if ($slide['multimedia_type'] == 'video') {
					$slideData['type']  = 'video';
					$slideData['video'] = $slide['src'];
				}

				echo "<li data-mightyslider='" . htmlspecialchars(trim(json_encode($slideData), '{}[]'), ENT_QUOTES, 'UTF-8') . "'>";

				$isContent = $slide['title'] || $slide['desc'];

				$title_size = empty($slide['extra']['title']['size']) ? '' : $slide['extra']['title']['size'];
				$title_color = empty($slide['extra']['title']['color']) ? '' : $slide['extra']['title']['color'];
				$desc_size = empty($slide['extra']['desc']['size']) ? '' : $slide['extra']['desc']['size'];
				$desc_color = empty($slide['extra']['desc']['color']) ? '' : $slide['extra']['desc']['color'];

				$title = '<span style="color:' . $title_color . '; font-size:' . $title_size . 'px;">' . $slide['title'] . '</span>';
				$desc   = '<span style="color:' . $desc_color . ';  font-size:' . $desc_size . 'px;">' . $slide['desc'] . '</span>';

				if ($isContent) : ?>
					<div class="paper_intro centralizeY" style="display: block;">
						<div class="paper_h"><?php echo $title; ?></div>
						<div class="paper_p"><?php echo $desc; ?></div>
						<div class="buttons">
							<?php
								$buttons =  $slide['extra']['buttons'];
								foreach ($buttons as $button){
									$corner_class = !empty($button['corner']) ? "button-{$button['corner']}" : '';
									$type_class = !empty($button['type']) ? "button-{$button['type']}" : '';
									$mode_class = !empty($button['mode']) ? "button-{$button['mode']}" : '';

									if (!empty($button['type'])){
										$style = 'border-color: ' . $button['color'] .  '; background-color: transparent;';
									}
									else {
										$style = 'background-color: ' . $button['color'] .  '; ';
									}

									echo '<a href="' .  $button['link'] . '" target="' . $button['target'] . '" class="button ' . $corner_class .' '. $type_class .' '. $mode_class . '" style="' . $style . '">' .
										'<span>' . $button['label'] . '</span>' .
										'</a>';
								}
							?>
						</div>
					</div>
				<?php endif;

				echo "</li>";
			}
			?>
		</ul>
	</div>
</div>

<?php

wp_enqueue_script(
	'mightyslider',
	get_template_directory_uri() . '/js/mightyslider.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_script(
	'tweenlite',
	get_template_directory_uri() . '/js/tweenlite.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_style(
	'mightyslider',
	get_template_directory_uri() . '/css/mightyslider/mightyslider.css',
	array(),
	'1.0'
);

?>

<script>
	jQuery(document).ready(function ($) {
		var isTouch = !!('ontouchstart' in window),
			clickEvent = isTouch ? 'tap' : 'click';


		(function () {
			var $carousel = $('#<?php echo $id; ?>'),
				$frame = $('.frame', $carousel),
				$caption = $('.caption', $carousel),
				$fullscreen = $('.fullscreen', $carousel),
				lastIndex = 0;

			if (!$carousel.length) {
				return;
			}

			var cycling = {};
			if ($frame.attr('data-cycling')) {
				cycling = {
					cycleBy: $frame.attr('data-cycling'),
					pauseTime: $frame.attr('data-cyclingDelay') * 1,
					loop:          1,    // Repeat cycling when last slide/page is activated.
					pauseOnHover:  1,    // Pause cycling when mouse hovers over the FRAME.
					startPaused:   0     // Whether to start in paused sate.
				};
			}

			$frame.mightySlider({
					speed: 1000,
					viewport: 'fill',
					easing: 'easeOutExpo',
					cycling: cycling,

					// Navigation options
					navigation: {
						slideSize: '100%',
						keyboardNavBy: 'slides'
					},

					// Dragging
					dragging: {
						mouseDragging: 0,
						touchDragging: 0
					},

					// Pages
					pages: {
						activateOn: clickEvent
					},

					// Buttons
					buttons: {
						fullScreen: $fullscreen
					},

					// Commands
					commands: {
						pages: 1,
						buttons: 1
					}
				},
				{
					active: function (name, index) {
						if (lastIndex === index)
							return false;

						var self = this,
							el = this.slides[index].element,
							$videos = $('video', $carousel),
							$video = $('video', el);
						video = $video[0];

						$videos.unbind('ended').each(function () {
							this.pause();
						});

						if (video && video.paused) {
							if (video.currentTime !== 0)
								video.currentTime = 0;

							video.play();

							$video.one('ended', function () {
								if (index === self.slides.length - 1)
									self.activate('0');
								else
									self.next();
							});
						}

						lastIndex = index;
					},
					coverLoaded: function (name, index) {
						var self = this,
							el = this.slides[index].element,
							$video = $('video', el);
						video = $video[0];

						if (video) {
							video.removeAttribute('loop');

							if (self.relative.activeSlide !== index)
								video.pause();
							else
								$video.one('ended', function () {
									if (index === self.slides.length - 1)
										self.activate('0');
									else
										self.next();
								});
						}
					},
					enterFullScreen: function () {
						$fullscreen.removeClass('icon-expand').addClass('icon-compress');
					},
					exitFullScreen: function () {
						$fullscreen.addClass('icon-expand').removeClass('icon-compress');
					}
				});

			$(document).on(clickEvent, '.paper_watch', function () {
				var $parent = $(this).parents('.mSSlide'),
					$playIcon = $('.mSVideo', $parent);

				$playIcon.trigger('click');
			});
		})();
	});
</script>
