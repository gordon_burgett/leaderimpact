<?php

$manifest = array();

$manifest['name']        = __( 'Carousel', 'fw' );
$manifest['description'] = __( 'Mighty Slider', 'fw' );
$manifest['version'] = '1.0.0';
$manifest['display'] = 'slider';
$manifest['standalone'] = true;
$manifest['requirements'] = array(
	'extensions' => array(
		'population-method-custom' => array(),
	)
);
