<?php
if (!defined('FW')) {
	die('Forbidden');
}

$id = uniqid('coverflow-');

$cycling = '';
if ($data['settings']['extra']['autoslide']) {
	$cycling = 'data-cycling="slides" data-cyclingDelay="' . $data['settings']['extra']['delay'] . '"';
}
$width = $dimensions['width'] ? 'width: ' . fw_theme_normalize_css_size($dimensions['width']) . ';' : '';
$height = $dimensions['height'] ? 'height: ' . fw_theme_normalize_css_size($dimensions['height']) . ';'  : '';

?>

<div class="mightySlider mightyslider_carouselCoverFlow_skin mightySlider_cover_flow" style="<?php echo $width; ?>" id="<?php echo $id; ?>">   <!-- PARENT -->
	<div class="frame" style="<?php echo $height; ?>" <?php echo $cycling?>>    <!-- FRAME -->
		<ul class="slideelement">   <!-- SLIDEELEMENT -->
			<?php

			foreach ($data['slides'] as $slide) {

				$slideData = array(
					'cover' => $slide['src'],
					'link'  => array( 'url' => $slide['src']),
					'title' => $slide['title'],
					'desc'  => $slide['desc'],
				);

				if ($slide['multimedia_type'] == 'video') {
					$slideData['type']  = 'video';
					$slideData['video'] = $slide['src'];
				}

				echo "<li data-mightyslider='" . htmlspecialchars(trim(json_encode($slideData), '{}[]'), ENT_QUOTES, 'UTF-8') . "'>";

				$isContent = $slide['title'] || $slide['desc'];

				$title_size = empty($slide['extra']['title']['size']) ? '' : $slide['extra']['title']['size'];
				$title_color = empty($slide['extra']['title']['color']) ? '' : $slide['extra']['title']['color'];
				$desc_size = empty($slide['extra']['desc']['size']) ? '' : $slide['extra']['desc']['size'];
				$desc_color = empty($slide['extra']['desc']['color']) ? '' : $slide['extra']['desc']['color'];

				$title = '<span style="color:' . $title_color . '; font-size:' . $title_size . 'px;">' . $slide['title'] . '</span>';
				$desc   = '<span style="color:' . $desc_color . ';  font-size:' . $desc_size . 'px;">' . $slide['desc'] . '</span>';

				echo "</li>";
			}
			?>
		</ul>
	</div>
	<div class="details">
		<div>
			<div class="slideTitle"></div>
			<div class="slideDesc"></div>
		</div>
	</div>
</div>
<?php

wp_enqueue_script(
	'mightyslider',
	get_template_directory_uri() . '/js/mightyslider.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_script(
	'tweenlite',
	get_template_directory_uri() . '/js/tweenlite.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_style(
	'mightyslider',
	get_template_directory_uri() . '/css/mightyslider/mightyslider.css',
	array(),
	'1.0'
);

?>

<script>
	jQuery(document).ready(function ($) {
		var $win = $(window),
			isTouch = !!('ontouchstart' in window),
			clickEvent = isTouch ? 'tap' : 'click';

		(function () {
			// Global slider's DOM elements
			var $carousel = $('#<?php echo $id; ?>'),
				$frame = $('.frame', $carousel),
				$title = $('div.details .slideTitle', $carousel),
				$desc = $('div.details .slideDesc', $carousel);

			if (!$carousel.length) {
				return;
			}

			var cycling = {};
			if ($frame.attr('data-cycling')) {
				cycling = {
					cycleBy: $frame.attr('data-cycling'),
					pauseTime: $frame.attr('data-cyclingDelay') * 1,
					loop:          1,    // Repeat cycling when last slide/page is activated.
					pauseOnHover:  1,    // Pause cycling when mouse hovers over the FRAME.
					startPaused:   0     // Whether to start in paused sate.
				};
			}

			// Calling mightySlider via jQuery proxy
			$frame.mightySlider({
					viewport: 'fill',
					startAt: 1,
					cycling: cycling,
					//autoScale: 1,

					// Navigation options
					navigation: {
						activateOn: clickEvent,
						slideSize: '50%'
					},

					// Commands options
					commands: {
						buttons: 1
					},

					// Scrolling options
					scrolling: {
						scrollBy: 1
					}
				},

				// Register callbacks to the events
				{
					// Register mightySlider :active event callback
					active: function (name, index) {
						$title.html(this.slides[index].options.title);
						$desc.html(this.slides[index].options.desc);
					}
				});
		})();
	});
</script>
