(function ($) {

    fwEvents.on('fw:options:init', function(e){
        makeDraggable(e);
    });

    var addItemTarget = null;
    $('body').on('click', '.add-new-item', function(e) {
        addItemTarget = $(e.target);
    });

    function makeDraggable(e) {
        var elSize = 15;

        // sync with values
        var syncValues = function($el) {
            var inputX = $el.parents('.pos-picker').find('.ui-picker-val-x');
            var inputY = $el.parents('.pos-picker').find('.ui-picker-val-y');
            var x = parseInt(inputX.val());
            var y = parseInt(inputY.val());

            // Extend from parent
            if (!x && !y) {
                if (addItemTarget && addItemTarget.is('.fw-option-box')) {
                    var prev$box = addItemTarget.prev();
                    if (prev$box.length) {
                        prev$box = $(prev$box[0]);
                        x = parseInt(prev$box.find('.pos-picker input.ui-picker-val-x').val());
                        y = parseInt(prev$box.find('.pos-picker input.ui-picker-val-y').val());
                    }
                } else if (addItemTarget && addItemTarget.is('.add-new-item')) {
                    var $box = addItemTarget.parents('.fw-option-box-options:eq(0)');
                    x = parseInt($box.find('.pos-picker input.ui-picker-val-x').val());
                    y = parseInt($box.find('.pos-picker input.ui-picker-val-y').val());
                }
                addItemTarget = null;

                if (!x && !y) {
                    x = '50%';
                    y = '50%';
                }

                inputX.val(x + '%');
                inputY.val(y + '%');
            }

            var parentX = $el.parent().innerWidth() - elSize;
            var parentY = $el.parent().innerHeight() - elSize;
            var valX = parentX / 100 * (x == 0 ? 1 : x);
            var valY = parentY / 100 * (y == 0 ? 1 : y);

            $el.css('top', valY);
            $el.css('left', valX);
        };

        if (e.$elements.length > 0 && $(e.$elements[0]).is('.fw-option-box')) {
            addItemTarget = $(e.$elements[0]);
        }

        $('.ui-picker-el')
            .draggable({
                containment: 'parent',
                drag: function (evt, ui) {
                    var $el = ui.helper,
                        x = $el.position().left,
                        y = $el.position().top,
                        parentX = $el.parent().innerWidth() - elSize,
                        parentY = $el.parent().innerHeight() - elSize,
                        percX = x / parentX * 100,
                        percY = y / parentY * 100;
                    $el.parents('.pos-picker').find('.ui-picker-val-x').val(Math.floor(percX) + '%');
                    $el.parents('.pos-picker').find('.ui-picker-val-y').val(Math.floor(percY) + '%');
                },
                create: function (evt, ui) {
                    syncValues($(evt.target));
                }
            })
            .each(function() {
                syncValues($(this));
            });

        $('.ui-picker-val-y').keyup(function () {
            $(this).parents('.pos-picker').find('.ui-picker-el').css('top', $(this).val());
        });
        $('.ui-picker-val-x').keyup(function () {
            $(this).parents('.pos-picker').find('.ui-picker-el').css('left', $(this).val());
        });
    }

})(jQuery);
