<?php
if (!defined('FW')) {
	die('Forbidden');
}

$id = uniqid('fullwidth');

$cycling = '';
if ($data['settings']['extra']['autoslide']) {
	$cycling = 'data-cycling="slides" data-cyclingDelay="' . $data['settings']['extra']['delay'] . '"';
}

$height = $dimensions['height'] ? 'height: ' . fw_theme_normalize_css_size($dimensions['height']) . ';'  : '';

?>

<div class="mightySlider mightyslider_modern_skin mightySlider_fullwidth" style="<?php echo $height; ?>" id="<?php echo $id; ?>">   <!-- PARENT -->
	<div class="frame" style="<?php echo $height; ?>" <?php echo $cycling; ?>>    <!-- FRAME -->
		<ul class="slideelement">   <!-- SLIDEELEMENT -->
			<?php

			foreach ($data['slides'] as $slide) {

				$slideData = array(
					'cover' => $slide['src'],
					'link'  => array( 'url' => $slide['src']),
					'title' => $slide['title'],
					'desc'  => $slide['desc'],
				);

				if ($slide['multimedia_type'] == 'video') {
					$slideData['type']  = 'video';
					$slideData['video'] = $slide['src'];
				}

				echo "<li data-mightyslider='" . htmlspecialchars(trim(json_encode($slideData), '{}[]'), ENT_QUOTES, 'UTF-8') . "'>";

				$isContent = $slide['title'] || $slide['desc'];

				$title_size = empty($slide['extra']['title']['size']) ? '' : $slide['extra']['title']['size'];
				$title_color = empty($slide['extra']['title']['color']) ? '' : $slide['extra']['title']['color'];
				$desc_size = empty($slide['extra']['desc']['size']) ? '' : $slide['extra']['desc']['size'];
				$desc_color = empty($slide['extra']['desc']['color']) ? '' : $slide['extra']['desc']['color'];

				$title = '<span style="color:' . $title_color . '; font-size:' . $title_size . 'px;">' . $slide['title'] . '</span>';
				$desc   = '<span style="color:' . $desc_color . ';  font-size:' . $desc_size . 'px;">' . $slide['desc'] . '</span>';

				if ($isContent) : ?>
					<div class="infoBlock infoBlockLeft">

						<div class="title"><?php echo $title;?></div>
						<p><?php echo $desc; ?></p>
						<div class="buttons"><?php

							$buttons =  $slide['extra']['buttons'];
							foreach ($buttons as $button){

								$corner_class = !empty($button['corner']) ? "button-{$button['corner']}" : '';
								$type_class = !empty($button['type']) ? "button-{$button['type']}" : '';
								$mode_class = !empty($button['mode']) ? "button-{$button['mode']}" : '';

								if (!empty($button['type'])){
									$style = 'border-color: ' . $button['color'] .  '; background-color: transparent;';
								}
								else {
									$style = 'background-color: ' . $button['color'] .  '; ';
								}

								echo '<a href="' .  $button['link'] . '" target="' . $button['target'] . '" class="button ' . $corner_class .' '. $type_class .' '. $mode_class . '" style="' . $style . '">' .
								'<span>' . $button['label'] . '</span>' .
								'</a>';

							}

							?>
						</div>
					</div>
				<?php endif;

				echo "</li>";
			}
			?>
		</ul>
	</div>
</div>

<script>
	jQuery(document).ready(function($){


	});
</script>

<?php

wp_enqueue_script(
	'mightyslider',
	get_template_directory_uri() . '/js/mightyslider.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_script(
	'tweenlite',
	get_template_directory_uri() . '/js/tweenlite.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_style(
	'mightyslider',
	get_template_directory_uri() . '/css/mightyslider/mightyslider.css',
	array(),
	'1.0'
);

?>

<script>
	jQuery(document).ready(function ($) {
		(function () {
			// Global slider's DOM elements
			var $slider = $('#<?php echo $id; ?>'),
				$frame = $('.frame', $slider);

			if (!$slider.length) {
				return;
			}

			var cycling = {};
			if ($frame.attr('data-cycling')) {
				cycling = {
					cycleBy: $frame.attr('data-cycling'),
					pauseTime: $frame.attr('data-cyclingDelay') * 1,
					loop:          1,    // Repeat cycling when last slide/page is activated.
					pauseOnHover:  1,    // Pause cycling when mouse hovers over the FRAME.
					startPaused:   0     // Whether to start in paused sate.
				};
			}

			// Calling mightySlider via jQuery proxy
			var slider = new mightySlider($frame, {
					speed: 1000,
					easing: 'easeOutExpo',
					viewport: 'fill',
					cycling: cycling,
					autoScale: 0,

					// Navigation options
					navigation: {
						slideSize: '100%',
						keyboardNavBy: 'slides'
					},

					// Dragging options
					dragging: {
						swingSpeed: 0.1
					},

					// Pages options
					pages: {
						activateOn: 'click'
					},

					// Commands options
					commands: {
						pages: 1,
						buttons: 1
					}
				},
				{
					load: function(){
						if ($slider.outerWidth() < 900){
							$slider.find('.infoBlockLeft').css({'left': '0', 'transform': 'translate(10%, -50%)'});
						}
					}

				}).init();
		})();
	});
</script>
