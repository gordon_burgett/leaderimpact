<?php
if (!defined('FW')) {
	die('Forbidden');
}

$id = uniqid('showcase-');

$cycling = '';
if ($data['settings']['extra']['autoslide']) {
	$cycling = 'data-cycling="slides" data-cyclingDelay="' . $data['settings']['extra']['delay'] . '"';
}

$width = $dimensions['width'] ? 'width: ' . fw_theme_normalize_css_size($dimensions['width']) . ';' : '';
$height = $dimensions['height'] ? 'height: ' . fw_theme_normalize_css_size($dimensions['height']) . ';'  : '';


?>

<div class="mightySlider mightyslider_carouselModern_skin mightySlider_showcase" style="<?php echo $width; ?> background: #000 url('<?php echo get_template_directory_uri(); ?>/css/curved_wooden.jpg') no-repeat 50%;" id="<?php echo $id; ?>">   <!-- PARENT -->
	<div class="frame" style="<?php echo $height; ?>" <?php echo $cycling?>>    <!-- FRAME -->
		<ul class="slideelement">   <!-- SLIDEELEMENT -->
			<?php

			foreach ($data['slides'] as $slide) {

				$slideData = array(
					'cover' => $slide['src'],
					'link'  => array( 'url' => $slide['src']),
					'title' => $slide['title'],
					'desc'  => $slide['desc'],
				);

				if ($slide['multimedia_type'] == 'video') {
					$slideData['type']  = 'video';
					$slideData['video'] = $slide['src'];
				}

				echo "<li data-mightyslider='" . htmlspecialchars(trim(json_encode($slideData), '{}[]'), ENT_QUOTES, 'UTF-8') . "'>";

				echo "</li>";
			}
			?>
		</ul>
	</div>
	<div class="details">
		<div>
			<div class="slideTitle"></div>
			<div class="slideDesc"></div>
		</div>
	</div>
</div>

<?php

wp_enqueue_script(
	'mightyslider',
	get_template_directory_uri() . '/js/mightyslider.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_script(
	'tweenlite',
	get_template_directory_uri() . '/js/tweenlite.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_style(
	'mightyslider',
	get_template_directory_uri() . '/css/mightyslider/mightyslider.css',
	array(),
	'1.0'
);

?>

<script>
	jQuery(document).ready(function ($) {
		var isTouch = !!('ontouchstart' in window),
			clickEvent = isTouch ? 'tap' : 'click',
			$win = $(window);

		(function () {

			function calculator(width){
				var percent = '50%';

				if (width <= 768) {
					percent = '90%';
				}

				return percent;
			}

			// Global slider's DOM elements
			var $carousel = $('#<?php echo $id; ?>'),
				$frame = $('.frame', $carousel),
				$details = $('div.details', $carousel),
				$title = $('div.details .slideTitle', $carousel),
				$desc = $('div.details .slideDesc', $carousel);

			if (!$carousel.length) {
				return;
			}

			var cycling = {};
			if ($frame.attr('data-cycling')) {
				cycling = {
					cycleBy: $frame.attr('data-cycling'),
					pauseTime: $frame.attr('data-cyclingDelay') * 1,
					loop:          1,    // Repeat cycling when last slide/page is activated.
					pauseOnHover:  1,    // Pause cycling when mouse hovers over the FRAME.
					startPaused:   0     // Whether to start in paused sate.
				};
			}


			// Calling mightySlider via jQuery proxy

			var slider = new mightySlider($frame, {
					speed: 2000,
					easing: 'easeOutExpo',
					viewport: 'fill',
					cycling: cycling,
					//autoScale: 1,

					// Navigation options
					navigation: {
						activateOn: clickEvent,
						slideSize: calculator($win.width())
					},

					// Buttons options
					buttons: {
						prev: $('.showcase_prev'),
						next: $('.showcase_next')
					},

					// Dragging options
					dragging: {
						swingSpeed: 0.1
					}

				},
				{
					// Register mightySlider :active event callback
					active: function (name, index) {
						if(this.slides[index].options.title || this.slides[index].options.desc){
							$details.show();
							$title.html(this.slides[index].options.title);
							$desc.html(this.slides[index].options.desc);
						} else {
							$details.hide();
						}
					}

				}).init();



			$win.resize(function(){
				slider.set({
					navigation: {
						slideSize: calculator($win.width())
					}
				});
			});
		})();
	});
</script>
