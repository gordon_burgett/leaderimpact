<?php
if (!defined('FW')) {
	die('Forbidden');
}

$id = uniqid('modern-');

$cycling = '';
if ($data['settings']['extra']['autoslide']) {
	$cycling = 'data-cycling="slides" data-cyclingDelay="' . $data['settings']['extra']['delay'] . '"';
}

$width = $dimensions['width'] ? 'width: ' . fw_theme_normalize_css_size($dimensions['width']) . ';' : '';
$height = $dimensions['height'] ? 'height: ' . fw_theme_normalize_css_size($dimensions['height']) . ';'  : '';

?>

<div class="mightySlider mightyslider_carouselModern_skin mightySlider_modern" style="<?php echo $width; ?>" id="<?php echo $id; ?>">   <!-- PARENT -->
	<div class="frame" <?php echo $cycling; ?> style="<?php echo $height; ?>">    <!-- FRAME -->
		<ul class="slideelement">   <!-- SLIDEELEMENT -->
			<?php

			foreach ($data['slides'] as $slide) {

				$slideData = array(
					'cover' => $slide['src'],
					'link'  => array( 'url' => $slide['src']),
					'title' => $slide['title'],
					'desc'  => $slide['desc'],
				);

				if ($slide['multimedia_type'] == 'video') {
					$slideData['type']  = 'video';
					$slideData['video'] = $slide['src'];
				}

				echo "<li data-mightyslider='" . htmlspecialchars(trim(json_encode($slideData), '{}[]'), ENT_QUOTES, 'UTF-8') . "'>";

				$isContent = $slide['title'] || $slide['desc'];

				$title_size = empty($slide['extra']['title']['size']) ? '' : $slide['extra']['title']['size'];
				$title_color = empty($slide['extra']['title']['color']) ? '' : $slide['extra']['title']['color'];
				$desc_size = empty($slide['extra']['desc']['size']) ? '' : $slide['extra']['desc']['size'];
				$desc_color = empty($slide['extra']['desc']['color']) ? '' : $slide['extra']['desc']['color'];

				$title = '<span style="color:' . $title_color . '; font-size:' . $title_size . 'px;">' . $slide['title'] . '</span>';
				$desc   = '<span style="color:' . $desc_color . ';  font-size:' . $desc_size . 'px;">' . $slide['desc'] . '</span>';

				if ($isContent) : ?>
					<div class="details">
						<div class="title"><?php echo $title; ?></div>
						<div class="description"><?php echo $desc; ?></div>
					</div>
				<?php endif;

				echo "</li>";
			}
			?>
		</ul>
	</div>
</div>
<?php

wp_enqueue_script(
	'mightyslider',
	get_template_directory_uri() . '/js/mightyslider.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_script(
	'tweenlite',
	get_template_directory_uri() . '/js/tweenlite.js',
	array( 'jquery' ),
	'1.0',
	true
);

wp_enqueue_style(
	'mightyslider',
	get_template_directory_uri() . '/css/mightyslider/mightyslider.css',
	array(),
	'1.0'
);

?>

<script>
	jQuery(document).ready(function ($) {
		var $win = $(window),
			isTouch = !!('ontouchstart' in window),
			clickEvent = isTouch ? 'tap' : 'click';

		(function () {
			/**
			 * Calculate the slides width in percent based on the parent's width.
			 *
			 * @return {String}
			 */
			function calculator(width) {
				var percent = '33.33%';

				if (width <= 768) {
					percent = '90%';
				}

				return percent;
			};

			// Global slider's DOM elements
			var $carousel = $('#<?php echo $id; ?>'),
				$frame = $('.frame', $carousel);

			if (!$carousel.length) {
				return;
			}

			var cycling = {};
			if ($frame.attr('data-cycling')) {
				cycling = {
					cycleBy: $frame.attr('data-cycling'),
					pauseTime: $frame.attr('data-cyclingDelay') * 1,
					loop:          1,    // Repeat cycling when last slide/page is activated.
					pauseOnHover:  1,    // Pause cycling when mouse hovers over the FRAME.
					startPaused:   0     // Whether to start in paused sate.
				};
			}

			// Calling new mightySlider class
			var slider = new mightySlider($frame, {
				speed: 1000,
				easing: 'easeOutExpo',
				viewport: 'fill',
				cycling: cycling,
				autoScale: 1,

				// Navigation options
				navigation: {
					navigationType: 'basic',
					activateOn: clickEvent,
					slideSize: calculator($win.width())
				},

				// Buttons options
				buttons: {
					prevPage: $('.modern_prev'),
					nextPage: $('.modern_next')
				}
			}).init();

			// Register window :resize event callback
			$win.resize(function () {
				// Update slider options using 'set' method
				slider.set({
					navigation: {
						slideSize: calculator($win.width())
					}
				});
			});
		})();
	});
</script>
