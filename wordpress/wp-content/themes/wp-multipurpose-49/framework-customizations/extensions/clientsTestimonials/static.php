<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

if ( ! is_admin() ) {
	global $template;
	/**
	 * @var FW_Extension_ClientsTestimonials $testimonials
	 */
	$testimonials = fw()->extensions->get( 'clientsTestimonials' );

	if ( is_singular( $testimonials->get_post_type_name() ) ) {
		wp_enqueue_style(
			'fw-extension-' . $testimonials->get_name() . '-nivo-default',
			$testimonials->locate_css_URI( 'NivoSlider/themes/default/default' ),
			array(),
			$testimonials->manifest->get_version()
		);

		wp_enqueue_style(
			'fw-extension-' . $testimonials->get_name() . '-nivo-dark',
			$testimonials->locate_css_URI( 'NivoSlider/themes/dark/dark' ),
			array(),
			$testimonials->manifest->get_version()
		);

		wp_enqueue_style(
			'fw-extension-' . $testimonials->get_name() . '-nivo-slider',
			$testimonials->locate_css_URI( 'nivo-slider' ),
			array(),
			$testimonials->manifest->get_version()
		);

		wp_enqueue_script(
			'fw-extension-' . $testimonials->get_name() . '-nivoslider',
			$testimonials->locate_js_URI( 'jquery.nivo.slider' ),
			array( 'jquery' ),
			$testimonials->manifest->get_version(),
			true
		);

		wp_enqueue_script(
			'fw-extension-' . $testimonials->get_name() . '-script',
			$testimonials->locate_js_URI( 'projects-script' ),
			array( 'fw-extension-' . $testimonials->get_name() . '-nivoslider' ),
			$testimonials->manifest->get_version(),
			true
		);
	}
}



