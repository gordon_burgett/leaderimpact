<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * Replace the content of the current template with the content of testimonials view
 *
 * @param string $the_content
 *
 * @return string
 */
function _filter_fw_ext_clientsTestimonials_the_content( $the_content ) {
	/**
 * @var FW_Extension_ClientsTestimonials $testimonials
	 */
	$testimonials = fw()->extensions->get( 'clientsTestimonials' );

	return fw_render_view( $testimonials->locate_view_path( 'hook-single' ), array( 'the_content' => $the_content ) );
}

/**
 * Check if the there are defined views for the clients templates, otherwise are used theme templates
 *
 * @param string $template
 *
 * @return string
 */
function _filter_fw_ext_clientsTestimonials_template_include( $template ) {

	/**
	 * @var FW_Extension_ClientsTestimonials $testimonials
	 */
	$testimonials = fw()->extensions->get( 'clientsTestimonials' );

	if ( is_singular( $testimonials->get_post_type_name() ) ) {
		if ( $testimonials->locate_view_path( 'single' ) ) {
			return $testimonials->locate_view_path( 'single' );
		} else {
			add_filter( 'the_content', '_filter_fw_ext_clientsTestimonials_the_content' );
		}
	} else if ( is_post_type_archive( $testimonials->get_post_type_name() ) && $testimonials->locate_view_path( 'archive' ) ) {
		return $testimonials->locate_view_path( 'archive' );
	}

	return $template;
}

add_filter( 'template_include', '_filter_fw_ext_clientsTestimonials_template_include' );

?>