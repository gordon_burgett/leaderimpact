<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'Testimonials', 'unyson' );
$manifest['description'] = __( "This extension will add a fully fledged testimonials module that will let you display your projects using the built in testimonials pages.", 'unyson' );
$manifest['version'] = '1.0.0';
$manifest['display'] = true;
$manifest['standalone'] = true;
$manifest['thumbnail'] = fw_locate_theme_path_uri('/framework-customizations/extensions/clientsTestimonials/static/images/thumbnail.png');