<?php

function FW_Extension_ClientsTestimonials_meta_box_ct_job_title($post) {
    ?>
    <input name="ct_job_title" id="ct_job_title" value="<?php echo get_post_meta($post->ID, 'ct_job_title', true) ?>" />
<?php
}

function FW_Extension_ClientsTestimonials_meta_box_ct_website_name($post) {
    ?>
    <input name="ct_website_name" id="ct_website_name" value="<?php echo get_post_meta($post->ID, 'ct_website_name', true) ?>" />
<?php
}

function FW_Extension_ClientsTestimonials_meta_box_ct_website_link($post) {
    ?>
    <input name="ct_website_link" id="ct_website_link" value="<?php echo get_post_meta($post->ID, 'ct_website_link', true) ?>" />
<?php
}