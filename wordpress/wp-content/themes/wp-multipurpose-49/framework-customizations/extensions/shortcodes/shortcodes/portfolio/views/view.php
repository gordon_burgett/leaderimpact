<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$rows = $atts['rows'];
$cols = $atts['columns'];

$post_amount = $rows * $cols;
?>


<script>
	jQuery(document).ready(function ($) {
		var $container = $('.portfolio-container');
		var spinner = '<div class="loader"></div>';
		$('.portfolio-category-link').click(function() {
			$container.addClass('loading');
			$('.loader').show();

			jQuery.ajax({
				type: 'GET',
				url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
				data: 'action=fw-portfolio&category=' + $(this).attr('data-category') + '&post_amount=' + <?php echo $post_amount; ?>,
				complete: function() {
					$container.removeClass('loading');
					$('.loader').hide();
				},
				success : function(data) {
					$container.html(data);
					$container.append(spinner);
				}
			});

			$('.portfolio-tabs .item').removeClass('active');

			$(this).parent().addClass('active');

		});

	});
</script>


<?php

$taxonomy = 'fw-portfolio-category';
$terms = get_terms($taxonomy);

if ( $terms && !is_wp_error( $terms ) ) :
	?>
	<ul class="portfolio-tabs">
			<li class="item active">
				<a href="javascript:void(0)" class="portfolio-category-link" data-category="">All</a>
			</li>
		<?php foreach ( $terms as $term ) { ?>
			<li class="item">
				<a href="javascript:void(0)" class="portfolio-category-link" data-category="<?php echo $term->slug ?>">
					<?php echo $term->name; ?>
				</a>
			</li>
		<?php } ?>
	</ul>
<?php endif;?>

<div class="portfolio-container">

	<?php
		$category_name = '';
		require '_items.php';
	?>

</div>
