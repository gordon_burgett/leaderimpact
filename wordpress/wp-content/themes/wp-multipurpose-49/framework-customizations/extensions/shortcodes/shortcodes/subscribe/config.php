<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Subscribe', 'fw' ),
	'description' => __( 'Create Subscribe Box', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' )
);