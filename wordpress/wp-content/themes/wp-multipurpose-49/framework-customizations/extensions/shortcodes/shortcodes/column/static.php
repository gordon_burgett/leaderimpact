<?php if (!defined('FW')) die('Forbidden');

wp_enqueue_style('fw-ext-builder-frontend-grid');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/column');

$shortcodes_extension = fw_ext('shortcodes');
wp_enqueue_style(
  'fw-shortcode-column-backround-video',
  // $shortcodes_extension->get_declared_URI('/shortcodes/column/static/css/jquery.fs.wallpaper.css')
  $uri . '/static/css/jquery.fs.wallpaper.css'
);
wp_enqueue_style(
  'fw-shortcode-column',
  // $shortcodes_extension->get_declared_URI('/shortcodes/column/static/css/styles.css')
  $uri . '/static/css/styles.css'
);

wp_enqueue_script(
  'fw-shortcode-column-backround-video',
  // $shortcodes_extension->get_declared_URI('/shortcodes/column/static/js/jquery.fs.wallpaper.js'),
  $uri . '/static/js/jquery.fs.wallpaper.js',
  array('jquery'),
  false,
  true
);
wp_enqueue_script(
  'fw-shortcode-column',
  // $shortcodes_extension->get_declared_URI('/shortcodes/column/static/js/scripts.js'),
  $uri . '/static/js/scripts.js',
  array('fw-shortcode-column-backround-video'),
  false,
  true
);
