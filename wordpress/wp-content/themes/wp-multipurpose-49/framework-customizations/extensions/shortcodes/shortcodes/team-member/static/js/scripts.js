jQuery(document).ready(function($){

  var inner = $( ".fw-team-member-inner" ),
      image = $( ".fw-team-member-image" );

  image.click(function(){
    
    var nextInner = $(this).next( inner );

    inner.each(function(){
      $(this).fadeOut(200);
    });

    image.each(function(){
      $(this).removeClass("active");
    });

    if (nextInner.is(":visible")) {
      nextInner.fadeOut(200);
      $(this).removeClass("active");
    }
    else {
      nextInner.fadeIn(200);
      $(this).addClass("active");
    }
    
  });
});
