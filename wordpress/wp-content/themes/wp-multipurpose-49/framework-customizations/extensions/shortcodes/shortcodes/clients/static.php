<?php if (!defined('FW')) die('Forbidden');

wp_enqueue_style('fw-ext-builder-frontend-grid');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/clients');

$shortcodes_extension = fw_ext('shortcodes');
wp_enqueue_style(
  'fw-shortcode-clients',
  $uri . '/static/css/styles.css'
);
