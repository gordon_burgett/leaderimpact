<?php if (!defined('FW')) die( 'Forbidden' ); ?>

<?php
$corner_class = !empty($atts['corner']) ? "button-{$atts['corner']}" : '';
$type_class = !empty($atts['type']) ? "button-{$atts['type']}" : '';
$mode_class = !empty($atts['mode']) ? "button-{$atts['mode']}" : '';



if (!empty($atts['type'])){
	$style = 'border-color: ' . $atts['color'] .  '; background-color: transparent;';
}
else {
	$style = 'background-color: ' . $atts['color'] .  '; ';
}

?>

<a href="<?php echo $atts['link'] ?>" target="<?php echo $atts['target'] ?>" class="button <?php echo $corner_class; ?> <?php echo $type_class; ?> <?php echo $mode_class; ?>" style="<?php echo $style; ?>">
	<span><?php echo $atts['label']; ?></span>
</a>