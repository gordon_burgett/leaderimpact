<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

$posts = get_posts( array(
		'post_type'      => 'fw-clients',
		'posts_per_page' => $atts['columns'] * $atts['rows'],
	)
);

$postCount = 0;
$endFlag = false;
?>

<?php if (!empty($posts)) { ?>

	<div class="fw-clients">

	<?php for ($row = 0; $row < $atts['rows']; $row++) {
		if ($endFlag) {
			break;
		}
	?>
		<div class="fw-clients-row">

		<?php for ($col = 0; $col < $atts['columns']; $col++) {
			if (isset($posts[$postCount])) {
				$post = $posts[$postCount];
				$postCount++;

				$link = get_post_meta($post->ID, 'client_link', true);

				if ($post->post_title != '') {
					$title = $post->post_title;
				} ?>

				<div class="fw-clients-col" style="width: <?php echo 100/$atts['columns']?>%;">

					<?php if ($link != '') { ?>
						<a href="//<?php echo $link ?>" target="_blank">
					<?php } ?>

						<img src="<?php echo fw_resize(get_post_thumbnail_id(intval($post->ID)), 0, 0) ?>" alt="<?php echo $title ?>">

					<?php if ($link != '') { ?>
						</a>
					<?php } ?>

				</div>

		<?php
			}
			else {
				$endFlag = true;
				break;
			}
		}
		?>
		</div>
	<?php } ?>
	</div>
<?php } ?>
