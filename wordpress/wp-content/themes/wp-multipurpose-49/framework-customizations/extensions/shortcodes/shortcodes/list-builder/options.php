<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'content'   => array(
                    'type'  => 'addable-option',
                    'value' => array(),
                    'label' => __('Content', 'fw'),
                    'option' => array('type' => 'text'),
    ),
    'type'  => array(
                'label'   => __('Type', 'fw'),
                'desc'    => __('Unordered, Circle, Ordered', 'fw'),
                'type'    => 'select',
                'choices' => array(
                    'unordered'  => __('Unordered', 'fw'),
                    'ordered'  => __('Ordered', 'fw')
                ),
                'value'   => 'unordered'
    ),
    'fontcolor' => array(
      'label' => __('Text Color', 'fw'),
      'desc'  => __('Please select the color', 'fw'),
      'type'  => 'color-picker',
      'value' => '#666666'
    ),
);

