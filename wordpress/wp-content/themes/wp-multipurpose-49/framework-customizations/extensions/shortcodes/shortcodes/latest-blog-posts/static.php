<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/latest-blog-posts');

wp_enqueue_script(
  'fw-shortcode-latest-blog-posts',
  $uri . '/static/js/scripts.js'
);

wp_enqueue_style(
  'fw-shortcode-latest-blog-posts',
  $uri . '/static/css/styles.css',
	array('fw-font-awesome')
);