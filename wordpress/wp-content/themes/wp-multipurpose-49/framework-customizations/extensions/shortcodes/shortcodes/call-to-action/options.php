<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'         => array(
		'type'  => 'text',
		'label' => __( 'Title', 'fw' ),
		'desc'  => __( 'This can be left blank', 'fw' )
	),
	'message'       => array(
		'type'  => 'textarea',
		'label' => __( 'Content', 'fw' ),
		'desc'  => __( 'Enter some content for this Info Box', 'fw' )
	),
	'button_label'  => array(
		'label' => __( 'Button Label', 'unyson' ),
		'desc'  => __( 'This is the text that appears on your button', 'unyson' ),
		'type'  => 'text',
		'value' => 'Click'
	),
	'button_color'  => array(
		'label'   => __( 'Button Color', 'unyson' ),
		'desc'    => __( 'Choose a color for action button', 'unyson' ),
		'type'    => 'color-picker',
		'value'		=> '#4eaade'
	),
	'button_link'   => array(
		'label' => __( 'Button Link', 'unyson' ),
		'desc'  => __( 'Where should your button link to', 'unyson' ),
		'type'  => 'text',
		'value' => '#'
	),
	'button_target' => array(
        'type'    => 'switch',
		'label'   => __( 'Open Link in New Window', 'unyson' ),
		'desc'    => __( 'Select here if you want to open the linked page in a new window', 'unyson' ),
        'right-choice' => array(
            'value' => '_blank',
            'label' => __('Yes', 'unyson'),
        ),
        'left-choice' => array(
            'value' => '_self',
            'label' => __('No', 'unyson'),
        ),
    ),
);