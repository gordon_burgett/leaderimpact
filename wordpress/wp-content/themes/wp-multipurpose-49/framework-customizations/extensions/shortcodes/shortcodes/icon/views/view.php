<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */
?>
<span class="fw-icon">
	<i style="color: <?php echo $atts['color']; ?>" class="<?php echo $atts['icon'] ?>"></i>
	<?php if (!empty($atts['title'])): ?>
		<br/>
		<span style="color: <?php echo $atts['color']; ?>" class="list-title"><?php echo $atts['title'] ?></span>
	<?php endif; ?>
</span>