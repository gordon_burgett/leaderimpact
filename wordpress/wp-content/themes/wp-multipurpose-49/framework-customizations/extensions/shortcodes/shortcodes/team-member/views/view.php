<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

if ( empty( $atts['image'] ) ) {
	$image = fw_get_framework_directory_uri('/static/img/no-image.png');
} else {
	$image = $atts['image']['url'];
}

$social = array(
	'facebook' => 'fa-facebook',
	'twitter' => 'fa-twitter',
	'linkedin' => 'fa-linkedin',
	'blog' => 'fa-rss',
	'google' =>'fa-google-plus',
	'email' => 'fa-envelope-o'
);

?>
<div class="fw-team-member">
	<div class="fw-team-member-image">
		<img src="<?php echo $image; ?>" alt="<?php echo $atts['name']; ?>">
	</div>
	<div class="fw-team-member-inner">
		<div class="fw-team-member-name">
			<h3><?php echo $atts['name']; ?></h3>
			<span><?php echo $atts['job']; ?></span>
		</div>
		<div class="fw-team-member-text">
			<p><?php echo $atts['desc']; ?></p>
		</div>
		<div class="fw-team-member-social">
			<?php	foreach ($social as $key => $value): ?>
				<a class="link" href="<?php echo $atts[$key]; ?>"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa <?php echo $value; ?> fa-stack-1x dark-blue"></i></span></a>
			<?php	endforeach; ?>
		</div>
	</div>
</div>
