<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$ruler_type = isset($atts['style']['ruler_type']) ? $atts['style']['ruler_type'] : 'line';

if ( 'line' === $ruler_type ): ?>
	<hr class="fw-divider-line" width="<?php echo isset($atts['style']['line']['width']) ? $atts['style']['line']['width'] : '100%'; ?>"
	    style="border-top-width: <?php echo isset($atts['style']['line']['height']) ? $atts['style']['line']['height'] : 1; ?>px"/>
<?php endif; ?>

<?php if ( 'space' === $ruler_type ): ?>
	<div class="fw-divider-space" style="padding-top: <?php echo (int) isset($atts['style']['space']['height']) ? $atts['style']['space']['height'] : 1; ?>px;"></div>
<?php endif; ?>