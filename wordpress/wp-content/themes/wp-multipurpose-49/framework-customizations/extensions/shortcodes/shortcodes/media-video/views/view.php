<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

global $wp_embed;

$id = 'video_' . uniqid();
$width = ( is_numeric( $atts['width'] ) && ( $atts['width'] > 0 ) ) ? $atts['width'] : '300';
$units = $atts['units'];
$iframe = $wp_embed->run_shortcode( '[embed]' . trim( $atts['url'] ) . '[/embed]' );

?>
<div class="video-wrapper shortcode-container" id="<?php echo $id; ?>">
	<?php echo do_shortcode( $iframe ); ?>
</div>
<script>
	jQuery(document).ready(function ($) {
		var wrapper 		= $('#<?php echo $id; ?>');
		var el 					= wrapper.children('iframe');
		var width 			= <?php echo $width; ?>;
		var height 			= Math.floor((width / 16) * 9);
		var units				= '<?php echo $units; ?>';

		if (units === 'pixel') {
			el.width(width);
			el.height(height);
		}
		else {
			el.css('width', width + '%');
			el.css('height', Math.floor((el.width() / 16) * 9));
			wrapper.css('height', Math.floor((el.width() / 16) * 9));
		}

		$(window).resize(function(){
			width 			= <?php echo $width; ?>;
			height 			= Math.floor((width / 16) * 9);
			if (units === 'pixel') {
				el.width(width);
				el.height(height);
			}
			else {
				el.css('width', width + '%');
				el.css('height', Math.floor((el.width() / 16) * 9));
				wrapper.css('height', Math.floor((el.width() / 16) * 9));
			}
		});
	});
</script>