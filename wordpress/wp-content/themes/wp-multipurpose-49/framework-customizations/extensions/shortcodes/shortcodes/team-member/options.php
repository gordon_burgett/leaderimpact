<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'image' => array(
		'label' => __( 'Team Member Image', 'unyson' ),
		'desc'  => __( 'Either upload a new, or choose an existing image from your media library', 'unyson' ),
		'type'  => 'upload'
	),
	'name'  => array(
		'label' => __( 'Team Member Name', 'unyson' ),
		'desc'  => __( 'Name of the person', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
	'job'   => array(
		'label' => __( 'Team Member Job Title', 'unyson' ),
		'desc'  => __( 'Job title of the person.', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
	'desc'  => array(
		'label' => __( 'Team Member Description', 'unyson' ),
		'type'  => 'textarea',
		'value' => ''
	),
	'facebook'  => array(
		'label' => __( 'Facebook', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
	'twitter'  => array(
		'label' => __( 'Twitter', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
	'linkedin'  => array(
		'label' => __( 'LinkedIn', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
	'blog'  => array(
		'label' => __( 'Blog', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
	'google'  => array(
		'label' => __( 'Google+', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
	'email'  => array(
		'label' => __( 'E-mail', 'unyson' ),
		'type'  => 'text',
		'value' => ''
	),
);