jQuery(document).ready(function($) {
  var win = $(window);

  var carousel = $('.latest_posts_slider > .frame');

      /**
    * Calculate the slides width in percent based on the parent's width.
    *
    * @return {String}
    */
    function calculator(width){
      var percent = '33.33%';

      if (width <= 480) {
        percent = '100%';
      }
      else if (width <= 768) {
        percent = '50%';
      }
      else if (width <= 980) {
        percent = '33.33%';
      }
      return percent;
    }

  slider = new mightySlider(carousel, {
      viewport: 'fill',
      startAt: 1,
      navigation: {
        keyboardNavBy: 'slides',
        slideSize: calculator(win.width()),
        navigationType: 'basic'
      },

      pages: {
        pagesBar:       'ul#latest_posts_nav',
        activateOn:     'click',
        pageBuilder: 
          function (index) {
            return '<li class="item"></li>';
          }
      }
    }).init();

  win.resize(function(){
    // Update slider options using 'set' method
    slider.set({
      navigation: {
        slideSize: calculator(win.width())
      }
    });
  }); 

});