<?php if ( ! defined( 'FW' ) ) {
  die( 'Forbidden' );
} ?>

<ul class="nav-dots" id="latest_posts_nav"></ul>
<div class="mightySlider mightyslider_carouselSimple_skin latest_posts_slider">
  <div class="frame">
    <ul class="slideelement">
      <?php 

      $the_query = new WP_Query(array(
        'posts_per_page' => 99,
        'post__not_in' => array( get_the_ID() )
        )
      );
      $post_counter = 0;
      ?>

      <?php while ($the_query -> have_posts()) : $the_query -> the_post();


        if ( has_post_thumbnail() && !($post_counter > 9) ) :
          ?>
        <li class="slide">
          <article id="post-<?php the_ID(); ?>" class="latest-blog-post">
            <div class="thumb-container">
              <?php
              if ( has_post_thumbnail() ) : ?>
              <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php 
                the_post_thumbnail('latest-posts-image', array('class' => 'thumb') );
                ?>
              </a>
            <?php else : ?>
              <div class="no_thumb"></div>
            <?php endif; ?>
            <div class="date">
              <div class="item day">
                <?php the_time('j'); ?>
              </div>
              <div class="item month text-uppercase">
                <?php the_time('M'); ?>
              </div>
            </div>
          </div>
          <div class="content">
            <?php
              the_title( '<h5 class="text-uppercase"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' );
            ?>
            <div class="meta">
              <i class="fa fa-lg pad-right fa-pencil-square-o"></i>By <?php the_author_posts_link(); ?><i class="fa fa-lg pad-right fa-folder-open-o"></i><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'unyson' ) ); ?>

            </div>
            <div class="summary">
              <?php 
              the_excerpt();
//              the_content('<span class="read-more">Read more <i class="fa fa-chevron-right"></i></span>');
              ?> 
            </div>
          </div>
        </article>
      </li>
      <?php endif;
        $post_counter++;
        ?>
    <?php endwhile;?>
    <?php wp_reset_postdata(); ?>

    </ul>
</div>
</div>

<script>
  jQuery(document).ready(function($){
    setTimeout(function(){
      var height = 0;
      $('.latest-blog-post').each(function(){
        var _this = $(this);
        if (_this.outerHeight() > height) {
          height = _this.outerHeight();
        }
      });
      $('.latest-blog-post').css('min-height', height);
    }, 1000);
  });
</script>


<?php

wp_enqueue_script(
  'mightyslider',
  get_template_directory_uri() . '/js/mightyslider.js',
  array( 'jquery' ),
  '1.0',
  true
);

wp_enqueue_script(
  'tweenlite',
  get_template_directory_uri() . '/js/tweenlite.js',
  array( 'jquery' ),
  '1.0',
  true
);
wp_enqueue_style(
  'mightyslider',
  get_template_directory_uri() . '/css/mightyslider/mightyslider.css',
  array(),
  '1.0'
);

?>
