<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Video', 'fw'),
	'description'   => __('Embed Video', 'fw'),
	'tab'           => __('Media Elements', 'fw'),
);
