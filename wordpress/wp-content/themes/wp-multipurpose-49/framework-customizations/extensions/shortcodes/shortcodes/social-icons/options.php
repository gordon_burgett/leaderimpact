<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
  'social'  => array(
    'label'   => __( 'Social', 'fw' ),
    'desc'    => __( 'Choose a social icon', 'fw' ),
    'type'    => 'select',
    'choices' => array(      
      'facebook'       => __( 'Facebook', 'fw' ),
      'google-plus'    => __( 'Google+', 'fw' ),
      'twitter'        => __( 'Twitter', 'fw' ),
      'foursquare'     => __( 'Foursquare', 'fw' ),
      'linkedin'       => __( 'LinkedIn', 'fw' ),
      'vk'             => __( 'VK', 'fw' ),
      'github'         => __( 'Github', 'fw' ),
      'reddit'         => __( 'Reddit', 'fw' ),
      'instagram'      => __( 'Instagram', 'fw' ),
      'skype'          => __( 'Skype', 'fw' ),
      'steam'          => __( 'Steam', 'fw' ),
      'trello'         => __( 'Trello', 'fw' ),
      'youtube'        => __( 'YouTube', 'fw' ),
      'tumblr'         => __( 'Tumblr', 'fw' ),
      'lastfm'         => __( 'LastFM', 'fw' ),
      'stack-overflow' => __( 'Stack Overflow', 'fw' ),
      'flickr'         => __( 'Flickr', 'fw' )
      ),
    ),
  'link'   => array(
    'label' => __( 'Button Link', 'fw' ),
    'desc'  => __( 'Where should your button link to', 'fw' ),
    'type'  => 'text',
    'value' => '#'
    ),
  'target' => array(
    'type'  => 'switch',
    'label'   => __( 'Open Link in New Window', 'fw' ),
    'desc'    => __( 'Select here if you want to open the linked page in a new window', 'fw' ),
    'right-choice' => array(
      'value' => '_blank',
      'label' => __('Yes', 'fw'),
      ),
    'left-choice' => array(
      'value' => '_self',
      'label' => __('No', 'fw'),
      ),
    ),
  'type' => array(
    'value' => '',
    'type'  => 'switch',
    'label'   => __( 'Round', 'fw' ),
    'desc'    => __( 'Select here if you want round icon', 'fw' ),
    'right-choice' => array(
      'value' => 'round',
      'label' => __('Yes', 'fw'),
      ),
    'left-choice' => array(
      'value' => '',
      'label' => __('No', 'fw'),
      ),
    ),
  'size'  => array(
    'label'   => __( 'Size', 'fw' ),
    'type'    => 'select',
    'choices' => array(      
      ''       => __( 'Normal', 'fw' ),
      'fa-lg'       => __( 'Large', 'fw' ),
      'fa-2x'       => __( '2x', 'fw' ),
      'fa-3x'       => __( '3x', 'fw' ),
      'fa-4x'       => __( '4x', 'fw' ),
      'fa-5x'       => __( '5x', 'fw' ),
      ),
    ),
  );