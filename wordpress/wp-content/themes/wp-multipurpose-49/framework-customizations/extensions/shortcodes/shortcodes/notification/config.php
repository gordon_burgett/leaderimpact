<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Notification', 'fw' ),
	'description' => __( 'A very awesome notification', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);