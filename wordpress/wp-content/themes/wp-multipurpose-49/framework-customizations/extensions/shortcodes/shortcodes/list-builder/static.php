<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/list-builder');

wp_enqueue_style(
  'fw-shortcode-list-builder',
  $uri . '/static/css/styles.css',
	array('fw-font-awesome')
);