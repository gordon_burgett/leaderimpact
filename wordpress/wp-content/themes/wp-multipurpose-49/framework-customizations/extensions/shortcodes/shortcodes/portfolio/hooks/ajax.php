<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}


add_action('wp_ajax_fw-portfolio', 'fw_shortcode_portfolio_ajax');
add_action('wp_ajax_nopriv_fw-portfolio', 'fw_shortcode_portfolio_ajax');

function fw_shortcode_portfolio_ajax() {
    $category_name = isset($_GET['category']) ? $_GET['category'] : '';
    $post_amount = isset($_GET['post_amount']) ? $_GET['post_amount'] : '-1';
    require dirname(__FILE__) . '/../views/_items.php';
    exit();
}
