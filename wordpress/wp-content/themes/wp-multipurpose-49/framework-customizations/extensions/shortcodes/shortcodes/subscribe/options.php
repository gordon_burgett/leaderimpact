<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'         => array(
		'type'  => 'text',
		'label' => __( 'Title', 'fw' ),
		'desc'  => __( 'This can be left blank', 'fw' )
	),
	'message'       => array(
		'type'  => 'textarea',
		'label' => __( 'Content', 'fw' ),
		'desc'  => __( 'Enter some content for this Subscribe Box', 'fw' )
	),
	'button_label'  => array(
		'label' => __( 'Button Label', 'fw' ),
		'desc'  => __( 'This is the text that appears on your button', 'fw' ),
		'type'  => 'text',
		'value' => 'Subscribe Now'
	),
	'username'  => array(
		'label' => __( 'Feed URL Name', 'fw' ),
		'desc'  => __( 'http://feeds2.feedburner.com/MyFeedName', 'fw' ),
		'type'  => 'text',
		'value' => ''
	),

);