<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>
<div class="fw-call-to-action">
	<div class="fw-action-content">
		<?php if (!empty($atts['title'])): ?>
		<h2><?php echo $atts['title']; ?></h2>
		<?php endif; ?>
		<p><?php echo $atts['message']; ?></p>
	</div>
	<div class="fw-action-btn">
		<a href="<?php echo isset($atts['button_link']) ? $atts['button_link'] : '#' ?>" class="button"
		   target="<?php echo isset($atts['button_target']) ? $atts['button_target'] : '' ?>"
		   <?php if (isset($atts['button_color'])): ?>
		        style="background-color: <?php echo $atts['button_color'] ?>;">
			<?php endif; ?>
			<span><?php echo isset($atts['button_label']) ? $atts['button_label'] : '&nbsp;' ?></span>
		</a>
	</div>
</div>