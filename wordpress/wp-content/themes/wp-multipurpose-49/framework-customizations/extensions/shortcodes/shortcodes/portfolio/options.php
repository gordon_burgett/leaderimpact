<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'rows'  => array(
		'label'   => __('Number of rows', 'fw'),
		'desc'    => __('', 'fw'),
		'type'    => 'select',
		'choices' => array(
			'1'  => __('1', 'fw'),
			'2'  => __('2', 'fw'),
			'3'  => __('3', 'fw'),
			'4'  => __('4', 'fw'),
			'5'  => __('5', 'fw')
		),
		'value'   => '1'
	),
	'columns'  => array(
		'label'   => __('Number of columns', 'fw'),
		'desc'    => __('', 'fw'),
		'type'    => 'select',
		'choices' => array(
			'1'  => __('1', 'fw'),
			'2'  => __('2', 'fw'),
			'3'  => __('3', 'fw'),
			'4'  => __('4', 'fw')
		),
		'value'   => '1'
	),
);