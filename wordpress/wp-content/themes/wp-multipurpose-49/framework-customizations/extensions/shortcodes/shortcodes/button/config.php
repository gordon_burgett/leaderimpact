<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Button', 'fw' ),
	'description' => __( 'A very awesome buttons', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);