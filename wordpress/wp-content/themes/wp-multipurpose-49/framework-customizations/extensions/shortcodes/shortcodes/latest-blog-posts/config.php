<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Latest Blog Posts', 'unyson' ),
	'description' => __( '', 'unyson' ),
	'tab'         => __( 'Media Elements', 'fw' ),
);