<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Icon', 'fw'),
	'description'   => __('A very awesome icon', 'fw'),
	'tab'           => __('Content Elements', 'fw'),
);