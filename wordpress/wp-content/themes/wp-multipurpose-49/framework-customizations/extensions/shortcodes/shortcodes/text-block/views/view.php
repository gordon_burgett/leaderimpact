<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>

<?php

$text_block = 'textblock_' . rand(1000, 999999);
?>
<style>
	#<?php echo $text_block ?> > p {
		color: <?php echo $atts['color']?> !important;
	}
</style>


<div id="<?php echo $text_block ?>"><?php echo $atts['text'];?></div>
