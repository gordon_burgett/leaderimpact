<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Layout Elements', 'fw'),
		'title'       => __('Section', 'fw'),
		'description' => __('Creates a section', 'fw'),
		'type'        => 'section' // WARNING: Do not edit this
	)
);