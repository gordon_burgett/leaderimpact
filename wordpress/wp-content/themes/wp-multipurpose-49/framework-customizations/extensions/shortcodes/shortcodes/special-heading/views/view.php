<?php if (!defined('FW')) die( 'Forbidden' );
/**
 * @var $atts
 */
?>
<div class="fw-heading fw-heading-<?php echo $atts['heading']; ?> <?php echo !empty($atts['centered']) ? 'fw-heading-center' : ''; ?>">
	<?php $heading = "<{$atts['heading']} class='fw-special-title' style='color: {$atts['color']};'>{$atts['title']}</{$atts['heading']}>"; ?>
	<?php echo $heading; ?>
	<?php if (!empty($atts['subtitle'])): ?>
		<div class="fw-special-subtitle" style="color: <?php echo $atts['color']; ?>"><?php echo $atts['subtitle']; ?></div>
	<?php endif; ?>
</div>