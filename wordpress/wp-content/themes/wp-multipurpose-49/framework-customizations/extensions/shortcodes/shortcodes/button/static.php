<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/button');

wp_enqueue_style(
	'fw-shortcode-button',
	// $shortcodes_extension->get_declared_URI('/shortcodes/button/static/css/styles.css')
  $uri . '/static/css/styles.css'
);

