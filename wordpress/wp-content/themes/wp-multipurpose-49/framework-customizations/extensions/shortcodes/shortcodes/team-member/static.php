<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/team-member');
wp_enqueue_script(
  'fw-shortcode-team-member',
  $uri . '/static/js/scripts.js'
);
wp_enqueue_style(
  'fw-shortcode-team-member',
  $uri . '/static/css/styles.css'
);