<?php if (!defined('FW')) die('Forbidden');

$options = array(
  'rows'  => array(
    'label'   => __('Number of rows', 'unyson'),
    'desc'    => __('', 'unyson'),
    'type'    => 'select',
    'choices' => array(
      '1'  => __('1', 'unyson'),
      '2'  => __('2', 'unyson'),
      '3'  => __('3', 'unyson'),
      '4'  => __('4', 'unyson'),
      '5'  => __('5', 'unyson')
    ),
    'value'   => '1'
  ),
  'columns'  => array(
    'label'   => __('Number of columns', 'unyson'),
    'desc'    => __('', 'unyson'),
    'type'    => 'select',
    'choices' => array(
      '1'  => __('1', 'unyson'),
      '2'  => __('2', 'unyson'),
      '3'  => __('3', 'unyson'),
      '4'  => __('4', 'unyson'),
      '5'  => __('5', 'unyson')
    ),
    'value'   => '1'
  ),
);