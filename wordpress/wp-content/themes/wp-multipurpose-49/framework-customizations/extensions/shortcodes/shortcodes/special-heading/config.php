<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Special Heading', 'fw'),
	'description'   => __('Make special headings', 'fw'),
	'tab'           => __('Content Elements', 'fw'),
);