<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/portfolio');

wp_enqueue_style(
	'fw-shortcode-portfolio',
  $uri . '/static/css/styles.css'
);

wp_enqueue_script(
  'fw-shortcode-portfolio',
  $uri . '/static/js/scripts.js'
);
