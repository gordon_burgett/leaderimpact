<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
  'color' => array(
    'type'  => 'color-picker',
    'value' => '#666666',
    'label' => __('Color', 'fw')
  ),
	'text' => array(
		'type'   => 'wp-editor',
		'teeny'  => true,
		'reinit' => true,
		'label'  => __( 'Content', 'fw' ),
		'desc'   => __( 'Enter some content for this texblock', 'fw' )
	)
);
