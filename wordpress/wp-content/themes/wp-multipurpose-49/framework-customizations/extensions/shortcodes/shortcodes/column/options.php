<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'background_color' => array(
		'label' => __('Background Color', 'fw'),
		'desc'  => __('Please select the background color', 'fw'),
		'type'  => 'color-picker',
	),
	'background_image' => array(
		'label'   => __('Background Image', 'fw'),
		'desc'    => __('Please select the background image', 'fw'),
		'type'    => 'background-image',
		'choices' => array(//	in future may will set predefined images
		)
	),
	'video' => array(
		'label' => __('Background Video', 'fw'),
		'desc'  => __('Insert Video URL to embed this video', 'fw'),
		'type'  => 'text',
	),
	'text_align' => array(
		'label' => __('Text align', 'fw'),
    'type'    => 'select',
    'choices' => array(
        'text-left'  => __('Left', 'fw'),
        'text-center'  => __('Center', 'fw'),
        'text-right'  => __('Right', 'fw')
    	),
    'value'   => 'text-left'
		)
);