<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>

<div class="fw-subscribe">
	<div class="fw-subscribe-content">
		<?php if (!empty($atts['title'])): ?>
			<h3><?php echo $atts['title']; ?></h3>
		<?php endif; ?>
		<p><?php echo $atts['message']; ?></p>
	</div>
	<div class="fw-subscribe-action">
		<form method="post" action="http://feedburner.google.com/fb/a/mailverify" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo $atts['username']; ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
			<div class="fw-input-container">
				<div class="fw-input-placeholder">
					<i class="fa fa-at fa-lg"></i>
				</div>
				<input class="fw-input email"  name="email" autocomplete="off" id="email" placeholder="Enter email" type="email" />
			</div>
			<input type="hidden" value="<?php echo $atts['username']; ?>" name="uri">
			<input type="hidden" name="loc" value="en_US">
			<input type="submit" class="button" value="<?php echo $atts['button_label'] ?>">
			</button>
		</form>
	</div>
</div>