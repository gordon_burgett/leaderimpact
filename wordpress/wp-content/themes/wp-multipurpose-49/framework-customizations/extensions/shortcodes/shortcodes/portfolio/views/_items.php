<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

?>
<?php


$args="post_type=fw-portfolio&fw-portfolio-category=" . $category_name . '&posts_per_page=' . $post_amount;

query_posts($args);

while ( have_posts()) : the_post(); ?>

    <div class="portfolio-item">
        <?php
        the_post_thumbnail('portfolio-preview');
        ?>
        <div class="desc">
            <div class="title">
                <?php
                the_title();
                ?>
            </div>
            <div class="portfolio-actions">
                <?php
                $image = wp_get_attachment_image_src( get_post_thumbnail_id(), "portfolio-preview" );
                ?>
                <a class="action portfolio-view" href="<?php echo $image[0]; ?>"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-search fa-stack-1x fa-inverse"></i></span></a><a class="action" href="<?php the_permalink(); ?>"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-link fa-stack-1x fa-inverse"></i></span></a>
            </div>
        </div>
    </div>

<?php
endwhile;
wp_reset_query();
?>