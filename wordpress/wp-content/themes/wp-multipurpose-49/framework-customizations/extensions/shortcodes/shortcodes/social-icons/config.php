<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Social Icons', 'fw' ),
	'description' => __( 'A very awesome social buttons', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);