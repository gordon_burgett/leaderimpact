<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Call To Action', 'fw' ),
	'description' => __( 'Create info boxes', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' )
);