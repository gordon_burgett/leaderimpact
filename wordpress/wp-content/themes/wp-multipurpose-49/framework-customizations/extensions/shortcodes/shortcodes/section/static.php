<?php if (!defined('FW')) die('Forbidden');

wp_enqueue_style('fw-ext-builder-frontend-grid');
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/section');

$shortcodes_extension = fw_ext('shortcodes');
wp_enqueue_style(
	'fw-shortcode-section-backround-video',
	$uri . '/static/css/jquery.fs.wallpaper.css'
);
wp_enqueue_style(
	'fw-shortcode-section',
	$uri . '/static/css/styles.css'
);

wp_enqueue_script(
	'fw-shortcode-section-backround-video',
	$uri . '/static/js/jquery.fs.wallpaper.js',
	array('jquery'),
	false,
	true
);
wp_enqueue_script(
	'fw-shortcode-section',
	$uri . '/static/js/scripts.js',
	array('fw-shortcode-section-backround-video'),
	false,
	true
);
