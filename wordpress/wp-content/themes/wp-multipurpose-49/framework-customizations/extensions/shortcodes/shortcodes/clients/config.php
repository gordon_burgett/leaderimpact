<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Clients', 'unyson' ),
	'description' => __( 'Clients block', 'unyson' ),
	'tab'         => __( 'Media Elements', 'fw' ),
);
