<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/table');

wp_enqueue_style(
	'fw-shortcode-table',
	// $shortcodes_extension->get_declared_URI('/shortcodes/table/static/css/styles.css')
  $uri . '/static/css/styles.css'
);
