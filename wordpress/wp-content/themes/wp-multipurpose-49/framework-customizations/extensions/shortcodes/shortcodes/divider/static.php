<?php if (!defined('FW')) die('Forbidden');

$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/divider');

wp_enqueue_style(
	'fw-shortcode-divider',
	$uri . '/static/css/styles.css'
);