<?php if (!defined('FW')) die( 'Forbidden' ); ?>
<?php $tabs_id = uniqid('fw-tabs-'); ?>
<?php
/*
 * the `.fw-tabs-container` div also supports
 * a `tabs-justified` class, that strethes the tabs
 * to the width of the whole container
 */
?>
<div class="fw-tabs-container" id="<?php echo $tabs_id ?>">
		<ul>
			<?php foreach ($atts['tabs'] as $key => $tab) : ?>
				<li data-href="<?php echo $tabs_id . '-' . ($key + 1); ?>"><a href="#"><?php echo $tab['tab_title']; ?></a></li>
			<?php endforeach; ?>
		</ul>
	<div class="tabs_container">
	<?php foreach ( $atts['tabs'] as $key => $tab ) : ?>
		<div class="fw-tab-content <?php echo $tabs_id . '-' . ($key + 1); ?>">
			<p><?php echo do_shortcode( $tab['tab_content'] ) ?></p>
		</div>
	<?php endforeach; ?>
	</div>
</div>

<script>
	jQuery(document).ready(function ($) {
		$("#<?php echo $tabs_id; ?>").tabulous();
	});

</script>