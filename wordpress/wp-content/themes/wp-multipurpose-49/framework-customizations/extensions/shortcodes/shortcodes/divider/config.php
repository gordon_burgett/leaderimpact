<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Divider', 'fw'),
	'description'   => __('A very awesome divider', 'fw'),
	'tab'           => __('Content Elements', 'fw'),
	'popup_size'    => 'small'
);