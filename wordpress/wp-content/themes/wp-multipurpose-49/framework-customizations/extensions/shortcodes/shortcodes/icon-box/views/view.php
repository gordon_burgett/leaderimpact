<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
?>
<?php
/*
 * `.fw-iconbox` supports 2 styles:
 * `fw-iconbox-1`, `fw-iconbox-2`
 */
?>
<div class="fw-iconbox clearfix <?php echo $atts['style']; ?>" style="color: <?php echo $atts['color']; ?>">
	<div class="fw-iconbox-image">
		<i class="<?php echo $atts['icon']; ?>"></i>
	</div>
	<div class="fw-iconbox-aside">
		<div class="fw-iconbox-title">
			<h3 style="color: <?php echo $atts['color']; ?>"><?php echo $atts['title']; ?></h3>
		</div>
		<div class="fw-iconbox-text">
			<p style="color: <?php echo $atts['color']; ?>"><?php echo $atts['content']; ?></p>
		</div>
	</div>
</div>