<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Accordion', 'fw' ),
	'description' => __( 'Create an accordion on the fly', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);
