<?php if (!defined('FW')) die('Forbidden'); ?>

<<?php echo $atts['type'] === 'ordered' ? 'ol' : 'ul'; ?>
	class="<?php echo isset($atts['type']) ? $atts['type'] : '' ?>"
	<?php if (isset($atts['fontcolor'])): ?>
	style="color: <?php echo $atts['fontcolor']; ?>"
	<?php endif; ?>
>

<?php foreach ($atts['content'] as $key => $value) { ?>
<li><?php echo $value; ?></li>
<?php } ?>

</<?php echo $atts['type'] === 'ordered' ? 'ol' : 'ul'; ?>>
