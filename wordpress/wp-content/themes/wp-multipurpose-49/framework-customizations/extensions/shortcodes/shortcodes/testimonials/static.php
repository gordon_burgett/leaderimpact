<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/testimonials');

wp_enqueue_style(
	'fw-shortcode-testimonials',
	// $shortcodes_extension->get_declared_URI('/shortcodes/testimonials/static/css/styles.css'),
  $uri . '/static/css/styles.css',
	array('fw-font-awesome')
);
wp_enqueue_script(
	'fw-shortcode-testimonials-caroufredsel',
	// $shortcodes_extension->get_declared_URI('/shortcodes/testimonials/static/js/jquery.carouFredSel-6.2.1-packed.js'),
  $uri . '/static/js/jquery.carouFredSel-6.2.1-packed.js',
	array('jquery'),
	false,
	true
);
