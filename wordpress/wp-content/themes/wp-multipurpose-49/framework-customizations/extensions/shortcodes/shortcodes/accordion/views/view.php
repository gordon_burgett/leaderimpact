<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>
<div class="fw-accordion">
	<?php foreach ( $atts['tabs'] as $tab ) : ?>
		<div class="fw-accordion-title"><?php echo $tab['tab_title']; ?></div>
		<div class="fw-accordion-content">
			<p><?php echo do_shortcode( $tab['tab_content'] ); ?></p>
		</div>
	<?php endforeach; ?>
</div>