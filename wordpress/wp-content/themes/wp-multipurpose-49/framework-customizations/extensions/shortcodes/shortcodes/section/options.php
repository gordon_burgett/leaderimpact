<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'background_color' => array(
		'label' => __('Background Color', 'fw'),
		'desc'  => __('Please select the background color', 'fw'),
		'type'  => 'color-picker',
	),
	'background_image' => array(
		'label'   => __('Background Image', 'fw'),
		'desc'    => __('Please select the background image', 'fw'),
		'type'    => 'background-image',
		'choices' => array(//	in future may will set predefined images
		)
	),
	'video' => array(
		'label' => __('Background Video', 'fw'),
		'desc'  => __('Insert Video URL to embed this video', 'fw'),
		'type'  => 'text',
	),
	'width'   => array(
		'label'   => __( 'Width', 'fw' ),
		'desc'    => __( 'Width of section', 'fw' ),
		'value'		=> 'fixed',
		'type'    => 'select',
		'choices' => array(
			'full' => __( 'Full', 'fw' ),
			'fixed'    => __( 'Fixed', 'fw' )
		)
	),
	'margin_top' => array(
		'label' => __('Margin Top', 'fw'),
		'desc'  => __('Set margins, Pixels only.', 'fw'),
		'type'  => 'text',
		'value'	=> '0'
	),
	'margin_bottom' => array(
		'label' => __('Margin Bottom', 'fw'),
		'desc'  => __('Set margins, Pixels only.', 'fw'),
		'type'  => 'text',
		'value'	=> '0'
	),
	'padding_top' => array(
		'label' => __('Padding Top', 'fw'),
		'desc'  => __('Set paddings, Pixels only.', 'fw'),
		'type'  => 'text',
		'value'	=> '0'
	),
	'padding_left' => array(
		'label' => __('Padding Left', 'fw'),
		'desc'  => __('Set paddings, Pixels only.', 'fw'),
		'type'  => 'text',
		'value'	=> '0'
	),
	'padding_bottom' => array(
		'label' => __('Padding Bottom', 'fw'),
		'desc'  => __('Set paddings, Pixels only.', 'fw'),
		'type'  => 'text',
		'value'	=> '0'
	),
	'padding_right' => array(
		'label' => __('Padding Right', 'fw'),
		'desc'  => __('Set paddings, Pixels only.', 'fw'),
		'type'  => 'text',
		'value'	=> '0'
	),

);
