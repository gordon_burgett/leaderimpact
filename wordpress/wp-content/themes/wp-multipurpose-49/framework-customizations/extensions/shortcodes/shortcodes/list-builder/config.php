<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => __( 'ListBuilder', 'fw' ),
		'description' => __( 'Create a list', 'fw' ),
		'tab'         => __( 'Content Elements', 'fw' ),
    'popup_size'  => 'small'
	)
);