<?php if (!defined('FW')) die('Forbidden');

$extra_classes = '';
$style = 'style="';
if (!empty($atts['background_color'])) {
	$style .= 'background-color:' . $atts['background_color'] . ';';
}

if (!empty($atts['background_image'])) {
	$style .= 'background-image:url(' . $atts['background_image']['data']['icon'] . ');';
}

$bg_video_data_attr = '';
if (!empty($atts['video'])) {
	$bg_video_data_attr = 'data-wallpaper-options=' . json_encode(array('source' => array('video' => $atts['video'])));
	$extra_classes .= ' background-video';
}


if ($atts['margin_top'] != 0) {
	$style .= ' margin-top:' . preg_replace("/[^0-9]/","",$atts['margin_top']). 'px; ';
}
if ($atts['margin_bottom'] != 0) {
	$style .= ' margin-bottom:' . preg_replace("/[^0-9]/","",$atts['margin_bottom']). 'px; ';
}
if ($atts['padding_top'] != 0) {
	$style .= ' padding-top:' . preg_replace("/[^0-9]/","",$atts['padding_top']). 'px; ';
}
if ($atts['padding_bottom'] != 0) {
	$style .= ' padding-bottom:' . preg_replace("/[^0-9]/","",$atts['padding_bottom']). 'px; ';
}
if ($atts['padding_left'] != 0) {
	$style .= ' padding-left:' . preg_replace("/[^0-9]/","",$atts['padding_left']). 'px; ';
}
if ($atts['padding_right'] != 0) {
	$style .= ' padding-right:' . preg_replace("/[^0-9]/","",$atts['padding_right']). 'px; ';
}
$style .= '"';

//if ($atts['width'] == 'fixed') $style .= 'padding-left: 0; padding-right: 0;';

?>


	<?php
		if ($atts['width'] == 'fixed') : ?>
<section class="fw-main-row <?php echo $extra_classes ?>" <?php echo $style; ?> <?php echo $bg_video_data_attr; ?>>
	<div class="fw-container">
		<?php echo do_shortcode($content); ?>
	</div>
</section>
	<?php else: ?>
<section class="fw-main-row-full <?php echo $extra_classes ?>" <?php echo $style; ?> <?php echo $bg_video_data_attr; ?>>
	<div class="fullwidth-section-content">
		<?php echo do_shortcode($content); ?>
	</div>
	</section>
	<?php endif; ?>
