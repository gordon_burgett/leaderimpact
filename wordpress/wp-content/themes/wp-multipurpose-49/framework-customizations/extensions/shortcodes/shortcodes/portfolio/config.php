<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Portfolio', 'fw' ),
	'description' => __( 'Add portfolio section', 'fw' ),
	'tab'         => __( 'Media Elements', 'fw' ),
);