<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/social-icons');

wp_enqueue_style(
	'fw-shortcode-social-icons',
	// $shortcodes_extension->get_declared_URI('/shortcodes/social-icons/static/css/styles.css')
  $uri . '/static/css/styles.css'
);

