<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'testimonials-' );
?>
<?php

	if ($atts['slider']) :
?>
<script>
	jQuery(document).ready(function ($) {
		$('#<?php echo $id ?>').carouFredSel({
			swipe: {
				onTouch: true
			},
			next : "#<?php echo $id ?>-next",
			prev : "#<?php echo $id ?>-prev",
			pagination : "#<?php echo $id ?>-controls",
			responsive: true,
			infinite: false,
			auto: false,
			debug: false,
			width: '100%',
			height: 'variable',
			items: {
				height: 'variable'
			},
			scroll: {
				items : 1,
				fx: "crossfade",
				easing: "linear",
				duration: 300
			},
			onWindowResize: 'updateSizes'
		});
	});
</script>
<?php
		$width = 100;
	else :
		$width = (100/$atts['visible'])-1;
	?>
		<style>
			.fw-testimonials-item{
				width: <?php echo $width; ?>%;
		}
		@media screen and (max-width: 768px){
			.fw-testimonials-item{
				width: 100%;
			}
		}
		</style>
<?php
	endif;
?>

<div class="fw-testimonials clearfix">
	<?php if (!empty($atts['title'])): ?>
		<h3 class="fw-testimonials-title"><?php echo $atts['title']; ?></h3>
	<?php endif; ?>

	<div class="fw-testimonials-list" id="<?php echo $id; ?>">
		<?php
			$args = array( 'post_type' => 'fw-testimonials', 'posts_per_page' => $atts['visible']);
			$loop = new WP_Query( $args);
			while ( $loop->have_posts() ) : $loop->the_post();
		?>
			<div class="fw-testimonials-item clearfix">
				<div class="fw-testimonials-text">
					<p><?php the_content(); ?></p>
				</div>
				<div class="fw-testimonials-meta">
					<div class="fw-testimonials-avatar">
						<?php
						$image_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
						$author_image_url = ($image_url != '')
											? $image_url
											: fw_get_framework_directory_uri('/static/img/no-image.png');
						?>
						<img src="<?php echo fw_resize($author_image_url, 50, 50, true); ?>" alt="<?php echo the_title(); ?>"/>
					</div>
					<div class="fw-testimonials-author">
						<span class="author-name"><?php echo the_title(); ?></span>
						<em><?php echo get_post_meta(get_the_ID(), 'ct_job_title', true) ?></em>
						<span class="fw-testimonials-url">
							<a href="//<?php echo get_post_meta(get_the_ID(), 'ct_website_link', true) ?>"><?php echo get_post_meta(get_the_ID(), 'ct_website_name', true) ?></a>
						</span>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
	</div>

	<div class="fw-testimonials-pagination" id="<?php echo $id; ?>-controls"></div>
</div>
