<?php if (!defined('FW')) die( 'Forbidden' );


?>


<a class="fw-social-icon" href="//<?php echo preg_replace('/^https?:\/\//', '', $atts['link']); ?>" target="<?php echo $atts['target']; ?>">
  <?php echo isset($atts['type']) ? '<span class="fa-stack ' . $atts['size'] . '"><i class="fa fa-circle fa-stack-2x fw-social-circle"></i>' : '';?>
    <i class="fa fa-<?php echo $atts['social']; ?> fa-stack-1x fw-social-ico"></i>
  <?php echo isset($atts['type']) ? '</span>' : '';?>
</a>  