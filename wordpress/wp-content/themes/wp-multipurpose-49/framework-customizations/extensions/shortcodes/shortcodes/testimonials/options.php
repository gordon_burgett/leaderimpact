<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title' => array(
		'label' => __( 'Title', 'fw' ),
		'desc'  => __( 'Option Testimonials Title', 'fw' ),
		'type'  => 'text',
	),
	'html' => array(
	'type'  => 'html',
	'value' => '',
	'label' => __('New', 'fw'),
	'html'  => '<a class="button button-primary page-builder-hide-button" href="edit.php?post_type=fw-testimonials" target="_blank">Add</a>',
	),
	'visible' => array(
		'type'  => 'select',
		'value' => '1',
		'label' => __('Visible', 'fw'),
		'desc'  => __('Amount of visible Testimonials', 'fw'),
		'choices' => array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			),
		),
	'slider' => array(
		'type'  => 'switch',
		'value' => 'true',
		'label' => __('Slider', 'fw'),
		'left-choice' => array(
			'value' => false,
			'label' => __('No', 'fw'),
		),
		'right-choice' => array(
			'value' => true,
			'label' => __('Yes', 'fw'),
		),
	)


);