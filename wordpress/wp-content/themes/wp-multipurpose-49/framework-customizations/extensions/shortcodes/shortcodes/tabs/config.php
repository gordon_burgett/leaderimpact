<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Tabs', 'fw' ),
	'description' => __( 'Create awesome tabs', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
);