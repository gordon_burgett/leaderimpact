<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'icon'       => array(
		'type' => 'icon',
		'label' => __( 'Icon', 'fw' )
	),
	'title'    => array(
		'type'  => 'text',
		'label' => __( 'Title', 'fw' ),
		'desc'  => __( 'Icon title', 'fw' ),
	),
	'color' => array(
		'label' => __('Color', 'fw'),
		'desc'  => __('Please select the color', 'fw'),
		'type'  => 'color-picker',
		'value' => '#4eaade'
	),
);