<?php if (!defined('FW')) die('Forbidden');

/** @internal */
function _filter_disable_shortcodes($to_disable)
{
	$to_disable[] = 'demo_disabled';
//	$to_disable[] = 'slider';
//	$to_disable[] = 'some_other_shortcode';
	return $to_disable;
}
add_filter('fw_ext_shortcodes_disable_shortcodes', '_filter_disable_shortcodes');

// Include hooks for ajax requests
require_once dirname(__FILE__) . '/shortcodes/portfolio/hooks/ajax.php';

function _fw_wpseo_pre_analysis_post_content($post_content) {
    fw_ext( 'shortcodes' )->_action_fw_extensions_init();
    return do_shortcode($post_content);
}
add_filter('wpseo_pre_analysis_post_content', '_fw_wpseo_pre_analysis_post_content');