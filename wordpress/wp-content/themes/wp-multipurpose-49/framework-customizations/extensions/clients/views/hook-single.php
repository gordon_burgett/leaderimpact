<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var string $the_content
 */

$atts['columns'] = $atts['columns'] ? $atts['columns'] : '1';

$fw_ext_projects_gallery_image = fw()->extensions->get( 'clients' )->get_config( 'image_sizes' );
$fw_ext_projects_gallery_image = $fw_ext_projects_gallery_image['featured-image'];

$thumbnails = fw_ext_clients_get_gallery_images();

$count = -1;
?>

<?php if (!empty($thumbnails)) {?>

	<div class="fw-clients">

		<?php foreach ($thumbnails as $thumbnail) { ?>
			<?
			$attachment = get_post( $thumbnail['attachment_id'] );
			$image = fw_resize( $thumbnail['attachment_id'], $fw_ext_projects_gallery_image['width'], $fw_ext_projects_gallery_image['height'], $fw_ext_projects_gallery_image['crop'] );
			$count++;
			?>
			<?php if ($count % $atts['columns'] == 0) { ?>
				<div class="fw-clients-row">
			<?php } ?>
			<div class="fw-clients-col">
				<img src="<?php echo $image ?>"
				     class="nivoslider-image"
				     alt="<?php echo $attachment->post_title ?>"
				     title="#nivoslider-caption-<?php echo $attachment->ID ?>"
				     width="<?php echo $fw_ext_projects_gallery_image['width'] ?>"
					/>
			</div>
			<?php if ($count % $atts['columns'] == 0) { ?>
				</div>
			<?php } ?>
		<?php } ?>

	</div>

<?php } ?>

<?php echo $the_content; ?>