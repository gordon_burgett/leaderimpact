<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'Clients', 'unyson' );
$manifest['description'] = __( "This extension will add a fully fledged clients  module that will let you display your projects using the built in clients pages.", 'unyson' );
$manifest['version'] = '1.0.0';
$manifest['display'] = true;
$manifest['standalone'] = true;
$manifest['thumbnail'] = fw_locate_theme_path_uri('/framework-customizations/extensions/clients/static/images/thumbnail.png');