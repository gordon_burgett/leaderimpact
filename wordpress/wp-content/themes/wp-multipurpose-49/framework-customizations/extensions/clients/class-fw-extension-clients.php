<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

class FW_Extension_Clients extends FW_Extension {

	private $post_type = 'fw-clients';
	private $slug = 'clients';

	/**
	 * @internal
	 */
	public function _init() {
		$this->define_slugs();

		add_action( 'init', array( $this, '_action_register_post_type' ) );

		if ( is_admin() ) {
			$this->add_admin_actions();
			$this->add_admin_filters();
		}
	}

	private function define_slugs() {
		$this->slug = apply_filters( 'fw_ext_clients_post_slug', $this->slug );
	}

	public function add_admin_actions() {
		// listing screen
		add_action( 'manage_' . $this->post_type . '_posts_custom_column', array(
			$this,
			'_action_admin_manage_custom_column'
		), 10, 2 );

		// add / edit screen
		add_action( 'do_meta_boxes', array( $this, '_action_admin_featured_image_label' ) );

		add_action( 'admin_enqueue_scripts', array( $this, '_action_admin_add_static' ) );

		add_action( 'admin_head', array( $this, '_action_admin_initial_nav_menu_meta_boxes' ), 999 );
	}

	public function add_admin_filters() {
		// filter seleector remove
		add_filter( 'months_dropdown_results', array( $this, '_filter_admin_remove_select_by_date_filter' ) );
		// items list view
		add_filter( 'manage_edit-' . $this->post_type . '_columns', array(
			$this,
			'_filter_admin_manage_edit_columns'
		), 10, 1 );
	}

	/**
	 * @internal
	 */
	public function _action_admin_add_static() {
		$clients_listing_screen  = array(
			'only' => array(
				array(
					'post_type' => $this->post_type,
					'base'      => array( 'edit' )
				)
			)
		);
		$clients_add_edit_screen = array(
			'only' => array(
				array(
					'post_type' => $this->post_type,
					'base'      => 'post'
				)
			)
		);

		if ( fw_current_screen_match( $clients_listing_screen ) ) {
			wp_enqueue_style(
				'fw-extension-' . $this->get_name() . '-listing',
				$this->get_declared_URI( '/static/css/admin-listing.css' ),
				array(),
				fw()->manifest->get_version()
			);
		}

		if ( fw_current_screen_match( $clients_add_edit_screen ) ) {
			wp_enqueue_style(
				'fw-extension-' . $this->get_name() . '-add-edit',
				$this->get_declared_URI( '/static/css/admin-add-edit.css' ),
				array(),
				fw()->manifest->get_version()
			);
			wp_enqueue_script(
				'fw-extension-' . $this->get_name() . '-add-edit',
				$this->get_declared_URI( '/static/js/admin-add-edit.js' ),
				array( 'jquery' ),
				fw()->manifest->get_version(),
				true
			);
		}
	}

	/**
	 * @internal
	 */
	public function _action_register_post_type() {

		$post_names = apply_filters( 'fw_ext_clients_post_type_name', array(
			'singular' => __( 'Client', 'unyson' ),
			'plural'   => __( 'Clients', 'unyson' )
		) );

		register_post_type( $this->post_type, array(
			'labels'             => array(
				'name'               => $post_names['plural'], //__( 'Clients', 'unyson' ),
				'singular_name'      => $post_names['singular'], //__( 'Clients project', 'unyson' ),
				'add_new'            => __( 'Add New', 'unyson' ),
				'add_new_item'       => sprintf( __( 'Add New %s', 'unyson' ), $post_names['singular'] ),
				'edit'               => __( 'Edit', 'unyson' ),
				'edit_item'          => sprintf( __( 'Edit %s', 'unyson' ), $post_names['singular'] ),
				'new_item'           => sprintf( __( 'New %s', 'unyson' ), $post_names['singular'] ),
				'all_items'          => sprintf( __( 'All %s', 'unyson' ), $post_names['plural'] ),
				'view'               => sprintf( __( 'View %s', 'unyson' ), $post_names['singular'] ),
				'view_item'          => sprintf( __( 'View %s', 'unyson' ), $post_names['singular'] ),
				'search_items'       => sprintf( __( 'Search %s', 'unyson' ), $post_names['plural'] ),
				'not_found'          => sprintf( __( 'No %s Found', 'unyson' ), $post_names['plural'] ),
				'not_found_in_trash' => sprintf( __( 'No %s Found In Trash', 'unyson' ), $post_names['plural'] ),
				'parent_item_colon'  => '' /* text for parent types */
			),
			'description'        => __( 'Create a clients item', 'unyson' ),
			'public'             => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'publicly_queryable' => true,
			/* queries can be performed on the front end */
			'has_archive'        => true,
			'rewrite'            => array(
				'slug' => $this->slug
			),
			'menu_position'      => 4,
			'show_in_nav_menus'  => false,
			'menu_icon'          => 'dashicons-groups',
			'hierarchical'       => false,
			'query_var'          => true,
			/* Sets the query_var key for this post type. Default: true - set to $post_type */
			'supports'           => array(
				'title',/* Text input field to create a post title. */
				'editor',
				'thumbnail', /* Displays a box for featured image. */
			),
			'capabilities'       => array(
				'edit_post'              => 'edit_pages',
				'read_post'              => 'edit_pages',
				'delete_post'            => 'edit_pages',
				'edit_posts'             => 'edit_pages',
				'edit_others_posts'      => 'edit_pages',
				'publish_posts'          => 'edit_pages',
				'read_private_posts'     => 'edit_pages',
				'read'                   => 'edit_pages',
				'delete_posts'           => 'edit_pages',
				'delete_private_posts'   => 'edit_pages',
				'delete_published_posts' => 'edit_pages',
				'delete_others_posts'    => 'edit_pages',
				'edit_private_posts'     => 'edit_pages',
				'edit_published_posts'   => 'edit_pages',
			),

			// Add additional fields
			'register_meta_box_cb' => array($this, '_register_meta_box_cb')
		) );

		add_filter( 'enter_title_here', array($this, '_filter_enter_title_here'), 10, 2 );
		add_filter( 'post_updated', array($this, '_filter_post_updated'), 10, 2 );
	}

	/**
	 * @internal
	 */
	public function _register_meta_box_cb() {
		// TODO: move to _action_admin_featured_image_label
		add_meta_box( 'client_link', __( 'Client Link', 'unyson' ), 'FW_Extension_Clients_meta_box_client_link', $this->post_type, 'normal' );
	}


	/**
	 * Change Title placeholder to Company Name
	 * @param string $title
	 * @param WP_Post $post
	 * @return string
	 * @internal
	 */
	public function _filter_enter_title_here($title, $post) {

		if ( 'fw-clients' == $post->post_type ){
			return __( 'Company Name', 'unyson' );
		}

		return $title;
	}

	/**
	 * Process additional fields
	 * @param integer $post_id
	 * @param WP_Post $post_after
	 * @internal
	 */
	public function _filter_post_updated($post_id, $post_after) {

		if ( 'fw-clients' == $post_after->post_type ) {
			// TODO: review this
			update_post_meta($post_id, 'client_link', $_POST['client_link']);
		}
	}

	/**
	 * Change the title of Featured Image Meta box
	 * @internal
	 */
	public function _action_admin_featured_image_label() {
		remove_meta_box( 'postimagediv', $this->post_type, 'side' );
		add_meta_box( 'postimagediv', __( 'Client Logo', 'unyson' ), 'post_thumbnail_meta_box', $this->post_type, 'side' );
	}

	/**
	 * @internal
	 *
	 * @param string $column_name
	 * @param int $id
	 */
	public function _action_admin_manage_custom_column( $column_name, $id ) {
		switch ( $column_name ) {
			case 'image':
				if ( get_the_post_thumbnail( intval( $id ) ) ) {
					$value = '<a href="' . get_edit_post_link( $id,
							true ) . '" title="' . esc_attr( __( 'Edit this item', 'unyson' ) ) . '">' .
					         '<img src="' . fw_resize( get_post_thumbnail_id( intval( $id ) ), 150, 100,
							true ) . '" width="150" height="100" >' .
					         '</a>';
				} else {
					$value = '<img src="' . $this->get_declared_URI( '/static/images/no-image.png' ) . '"/>';
				}
				echo $value;
				break;

			default:
				break;
		}
	}

	/**
	 * @internal
	 */
	public function _action_admin_initial_nav_menu_meta_boxes() {
		$screen = array(
			'only' => array(
				'base' => 'nav-menus'
			)
		);
		if ( ! fw_current_screen_match( $screen ) ) {
			return;
		}

		if ( get_user_option( 'fw-metaboxhidden_nav-menus' ) !== false ) {
			return;
		}

		$user              = wp_get_current_user();
		$hidden_meta_boxes = get_user_meta( $user->ID, 'metaboxhidden_nav-menus' );

		update_user_option( $user->ID, 'metaboxhidden_nav-menus', $hidden_meta_boxes[0], true );
		update_user_option( $user->ID, 'fw-metaboxhidden_nav-menus', 'updated', true );
	}

	/**
	 * @internal
	 *
	 * @param array $columns
	 *
	 * @return array
	 */
	public function _filter_admin_manage_edit_columns( $columns ) {
		$new_columns          = array();
		$new_columns['cb']    = $columns['cb']; // checkboxes for all projects page
		$new_columns['image'] = __( 'Cover Image', 'unyson' );

		return array_merge( $new_columns, $columns );
	}

	/**
	 * @internal
	 *
	 * @param array $filters
	 *
	 * @return array
	 */
	public function _filter_admin_remove_select_by_date_filter( $filters ) {
		return array();
		$screen = array(
			'only' => array(
				'base' => 'edit',
				'id'   => 'edit-' . $this->post_type,
			)
		);

		if ( ! fw_current_screen_match( $screen ) ) {
			return $filters;
		}

		return array();
	}

	/**
	 * @internal
	 *
	 * @return string
	 */
	public function _get_link() {
		return 'edit.php?post_type=' . $this->post_type;
	}

	public function get_settings() {

		$response = array(
			'post_type'     => $this->post_type,
			'slug'          => $this->slug,
		);

		return $response;
	}

	public function get_image_sizes() {
		return $this->get_config( 'image_sizes' );
	}

	public function get_post_type_name() {
		return $this->post_type;
	}
}
