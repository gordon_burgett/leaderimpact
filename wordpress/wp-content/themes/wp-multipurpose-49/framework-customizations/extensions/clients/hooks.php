<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * Replace the content of the current template with the content of clients view
 *
 * @param string $the_content
 *
 * @return string
 */
function _filter_fw_ext_clients_the_content( $the_content ) {
	/**
	 * @var FW_Extension_Clients $clients
	 */
	$clients = fw()->extensions->get( 'clients' );

	return fw_render_view( $clients->locate_view_path( 'hook-single' ), array( 'the_content' => $the_content ) );
}

/**
 * Check if the there are defined views for the clients templates, otherwise are used theme templates
 *
 * @param string $template
 *
 * @return string
 */
function _filter_fw_ext_clients_template_include( $template ) {

	/**
	 * @var FW_Extension_Clients $clients
	 */
	$clients = fw()->extensions->get( 'clients' );

	if ( is_singular( $clients->get_post_type_name() ) ) {
		if ( $clients->locate_view_path( 'single' ) ) {
			return $clients->locate_view_path( 'single' );
		} else {
			add_filter( 'the_content', '_filter_fw_ext_clients_the_content' );
		}
	} else if ( is_post_type_archive( $clients->get_post_type_name() ) && $clients->locate_view_path( 'archive' ) ) {
		return $clients->locate_view_path( 'archive' );
	}

	return $template;
}

add_filter( 'template_include', '_filter_fw_ext_clients_template_include' );

?>