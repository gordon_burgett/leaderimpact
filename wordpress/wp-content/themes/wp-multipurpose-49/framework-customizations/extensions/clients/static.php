<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

if ( ! is_admin() ) {
	global $template;
	/**
	 * @var FW_Extension_Clients $clients
	 */
	$clients = fw()->extensions->get( 'clients' );

	if ( is_singular( $clients->get_post_type_name() ) ) {
		wp_enqueue_style(
			'fw-extension-' . $clients->get_name() . '-nivo-default',
			$clients->locate_css_URI( 'NivoSlider/themes/default/default' ),
			array(),
			$clients->manifest->get_version()
		);

		wp_enqueue_style(
			'fw-extension-' . $clients->get_name() . '-nivo-dark',
			$clients->locate_css_URI( 'NivoSlider/themes/dark/dark' ),
			array(),
			$clients->manifest->get_version()
		);

		wp_enqueue_style(
			'fw-extension-' . $clients->get_name() . '-nivo-slider',
			$clients->locate_css_URI( 'nivo-slider' ),
			array(),
			$clients->manifest->get_version()
		);

		wp_enqueue_script(
			'fw-extension-' . $clients->get_name() . '-nivoslider',
			$clients->locate_js_URI( 'jquery.nivo.slider' ),
			array( 'jquery' ),
			$clients->manifest->get_version(),
			true
		);

		wp_enqueue_script(
			'fw-extension-' . $clients->get_name() . '-script',
			$clients->locate_js_URI( 'projects-script' ),
			array( 'fw-extension-' . $clients->get_name() . '-nivoslider' ),
			$clients->manifest->get_version(),
			true
		);
	}
}



