<?php

get_header(); ?>

  <div id="main-content" class="main-content">
    <div id="primary" class="content-area <?php with_sidebar(); ?>">
      <div id="content" class="site-content " role="main">
        <div class="fw-container">
        <?php woocommerce_content(); ?>
        </div>
      </div><!-- #content -->
    </div><!-- #primary -->
    <?php	get_sidebar(); ?>

  </div><!-- #main-content -->

<?php
get_footer();
