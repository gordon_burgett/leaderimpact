<?php
/**
 * The Template for displaying all single posts
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area <?php with_sidebar(); ?>">
		<div id="content" class="site-content" role="main">

				<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

						?>
						<div class="fw-container">
							<?php
							// Previous/next post navigation.
							fw_theme_post_nav();
							?>
						</div>
						<?php

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
					endwhile;
				?>

		</div><!-- #content -->
	</div><!-- #primary -->
		<?php get_sidebar(); ?>
</div>
<?php
get_footer();
